#!/usr/bin/env python
"""
This script allows conversion from one table format to another
"""

import argparse
import sys

import astro_utils.table_utils as tabu


def main():
    """
    Allow the entry of command line arguments and calls convert_table function
    """
    parser = argparse.ArgumentParser(description=("Convert an input table from"
                                                  "one format to another,"
                                                  "with sorting and column"
                                                  "selection"))
    parser.add_argument("input_file", type=str, default=sys.stdin,
                        help=("A file containing a table of a type readable"
                              "by astropy.io or a .lst file"))
    parser.add_argument('output_file', type=str, default=sys.stdout,
                        help="A file to write the table to")
    parser.add_argument("--order", type=str, nargs='+', default=None,
                        help="The order of columns desired in the output file")
    parser.add_argument("--sort", type=str, nargs='+', default=None,
                        help=("The columns to sort the rows by, the priority"
                              "decreasing to the right"))
    parser.add_argument("--coordinate_format", type=str, default='decimal',
                        help=("The desired format of positions in the output"
                              "file, either 'decimal' or 'sexagesimal'"
                              "(default None: returns the input format)"))
    parser.add_argument("--dp", type=int, default=5,
                        help=("The desired number of decimal places for both"
                              "right ascension and declination"))
    parser.add_argument("--dp_ra", type=int, default=None,
                        help=("The desired number of right ascension decimal"
                              "places (default None)"))
    parser.add_argument("--dp_dec", type=int, default=None,
                        help=("The desired number of declination decimal"
                              "places (default None)"))
    args = parser.parse_args()

    tabu.convert_table(input_file=args.input_file,
                       output_file=args.output_file,
                       order_list=args.order, sort_list=args.sort,
                       coordinate_format=args.coordinate_format,
                       decimal_places=args.dp,
                       decimal_places_ra=args.dp_ra,
                       decimal_places_dec=args.dp_dec)

if __name__ == '__main__':
    main()
