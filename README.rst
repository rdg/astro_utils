astro_utils
===========

astro_utils provides modules to assist in astronomical work.
It is most useful for tasks involving spectra and tables.
Typical usage often looks like this::

    #!/usr/bin/env python

    import scipy.constants as con

    from astro_utils import spectra_utils as specu
    from astro_utils import plot_utils as plotu

    spec = specu.get_hipe_spectrum_from_file('hipe_spectrum.fits')
    specu.blueshift(spec, 2.213)
    specu.convert_dispersion(spec, um_Ghz_um)

    figure = plotu.plot_spectrum_to_figure(spec, disp_range=(110, 130))
    plotu.add_spectral_lines(figure, font_size=8)
    figure.savefig('figure.pdf')


Modules Included
----------------

The package contains the following modules:

* ``astro_utils.spectra_utils``

* ``astro_utils.plot_utils``

* ``astro_utils.table_utils``

* ``astro_utils.moving``


Programs Included
-----------------

The package installs the following stand-alone scripts into /usr/bin or similar

* ``convert_coordinates``

* ``convert_table``

