"""
"""
from __future__ import division, print_function
import copy

import astropy.constants.si as con
from astropy.table import Table
import astropy.units as u
import numpy as np
from scipy.optimize import curve_fit, minimize

from astro_utils.spec_utils import *
import astro_utils.maths_utils as mathsu


def extract_properties_from_fit(fit_function, optimal_parameters, z, **kwargs):
    """
    """
    magnification = kwargs.get('magnification', 1)
    disp_low = kwargs.get('output_spectrum_disp_low', 1e8 * u.Hz)
    disp_high = kwargs.get('output_spectrum_disp_high', 1e15 * u.Hz)
    disp_step = kwargs.get('output_spectrum_disp_step', None)
    num_steps = kwargs.get('output_spectrum_num_steps', None)

    # Sample fitted spectrum at more frequencies for luminosity calculation
    disp = np.logspace(8, 15, num=1000)
    flux = 10 ** fit_function(disp, *optimal_parameters)
    fit_spectrum = Spectrum1D(disp=disp * u.Hz,
                              flux=flux * u.W / u.m ** 2 / u.Hz)

    # lum_density is a Spectrum1D
    lum_density = flux_density_to_luminosity_density(fit_spectrum, z,
                                                     magnification=\
                                                         magnification)
    l_bol = bolometric_luminosity(lum_density)
    l_fir = fir_luminosity(lum_density)
    l_ir = ir_luminosity(lum_density)
    l_40_500 = ir_luminosity(lum_density,
                             lower_disp=(40 * u.um), upper_disp=(500 * u.um))
    l_60 = lum_density.interpolate_flux(60 * u.um)
    l_100 = lum_density.interpolate_flux(100 * u.um)

    # Far-infrared radio correlation
    q_ir = fir_radio_correlation(
            l_fir, lum_density.interpolate_flux(1.4 * u.GHz))

    ## Format output spectrum with dispersion values passed as kwargs
    ## Units to calculate dispersion values in (not necessarily output values)
    #if disp_step is not None and num_steps is None:
    #    step_input = True
    #    unit = disp_step.unit
    #else:
    #    step_input = False
    #    unit = disp_low.unit

    ## Determine end points of output spectrum
    #try:
    #    disp_low = disp_low.to(unit)
    #except u.UnitsException:
    #    disp_low = disp_low.to(unit, equivalencies=u.spectral())
    #try:
    #    disp_high = disp_high.to(unit)
    #except u.UnitsException:
    #    disp_high = disp_high.to(unit, equivalencies=u.spectral())

    ## Create dispersion array
    #if step_input:
    #    disp = np.arange(disp_low, disp_high, disp_step)
    #    try:
    #        disp = unit.to(disp_low.unit, disp)
    #    except u.UnitsException:
    #        disp = unit.to(disp_low.unit, disp, equivalencies=u.spectral())
    #else:  # Unit is that of disp_low
    #    if num_steps is None:
    #        #print("Neither a number of steps or step size for the output "
    #        #      "spectrum has been given, giving 1000 steps"            )
    #        num_steps = 1000
    #    disp = np.logspace(np.log10(disp_low.value),
    #                       np.log10(disp_high.value), num_steps)

    ## Calculate flux and create output_spectrum
    #flux = 10 ** fit_function(disp, *optimal_parameters)
    #output_spectrum = Spectrum1D(disp=disp * disp_low.unit,
    #                             flux=flux * u.W / u.m ** 2 / u.Hz)
    output_spectrum = fit_spectrum

    properties = {
        'bolometric_luminosity': l_bol,
        'bolometric_luminosity_mag': l_bol * magnification,
        'fir_luminosity': l_fir,
        'fir_luminosity_mag': l_fir * magnification,
        'ir_luminosity': l_ir,
        'ir_luminosity_mag': l_ir * magnification,
        '40-500_luminosity': l_40_500,
        '40-500_luminosity_mag': l_40_500 * magnification,
        '60_luminosity_density': l_60,
        '60_luminosity_density_mag': l_60 * magnification,
        '100_luminosity_density': l_100,
        '100_luminosity_density_mag': l_100 * magnification,
        'fir_radio_correlation': q_ir,
        'spectrum': output_spectrum,
        }
    return properties


def fit_one_temp(spectrum, area, z, **kwargs):
    """
    Fit a single grey body and synchrotron emission to `spectrum`.

    The synchrotron flux is given by:
    flux = synchrotron_scalar * freq ** (-synchrotron_spectral_index),

    Parameters
    ----------
    spectrum : astro_utils.spectra_utils.Spectrum1D or similar
        Contains ``disp``, ``flux`` astropy.nddata.NDData attributes.
    area : float
        The projected area of the source in m**2.
    z : float
        The redshift of the source.
    magnification : float, optional
        Flux magnification factor from gravitational lensing, (default=``1``).
    output_spectrum_disp_low :
    output_spectrum_disp_high :
    spectral_emissivity_index : float, optional
        Often denoted as 'beta', (default=``1.8``).
    synchrotron_scalar : float, optional
        Initial guess, (default=``1e30``).
    synchrotron_spectral_index : float, optional
        Initial guess, (default=``0.75``).
    temperature : float, optional
        Initial guess of the temperature of the emitting dust in K,
        (default=``30``).
    total_dust_mass : float, optional
        Initial guess of the mass of dust within the source in kg,
        (default=``2e39``, 1e9 * M_sun).

    Returns
    -------

    """
    magnification = kwargs.get('magnification', 1)
    disp_low = kwargs.get('output_spectrum_disp_low', 1e8 * u.Hz)
    disp_high = kwargs.get('output_spectrum_disp_high', 1e15 * u.Hz)
    disp_step = kwargs.get('output_spectrum_disp_step', None)
    fit_magnification = kwargs.get('fit_magnification', False)
    num_steps = kwargs.get('output_spectrum_num_steps', None)
    spectral_emissivity_index = kwargs.get('spectral_emissivity_index', 1.8)
    synchrotron_scalar = kwargs.get('synchrotron_scalar', 1e37)
    synchrotron_spectral_index = kwargs.get('synchrotron_spectral_index', -0.75)
    temperature = kwargs.get('temperature', 30)
    total_dust_mass = kwargs.get('total_dust_mass', 2e39)

    # Fit the synchrotron component first
    popt_sync, pcov_sync = fit_synchrotron(spectrum, z, **kwargs)

    # Now fit a grey body + the previously calculated synchrotron spectrum.
    # Create a new function accepting only the fitting parameters
    #def log10_one_grey_body(disp, spectral_emissivity_index,
    #                        temperature, total_dust_mass    ):
    def log10_one_grey_body(disp, *args):
        """ A single grey body + synchrotron spectrum to fit this source. """
        nonlocal magnification
        if fit_magnification:
            (mag, spectral_emissivity_index,
                    temperature, total_dust_mass      ) = args
        else:
            mag = copy.copy(magnification)
            spectral_emissivity_index, temperature, total_dust_mass = args

        # Luminosity density spectrum
        flux_density = (observed_grey_body(disp,
                                           area=area,
                                           magnification=mag,
                                           spectral_emissivity_index=\
                                               spectral_emissivity_index,
                                           temperature=temperature,
                                           total_dust_mass=total_dust_mass,
                                           z=z
                                           ).flux.value
                        + observed_synchrotron(disp,
                                               magnification=magnification,
                                               scalar=popt_sync[0],
                                               spectral_index=popt_sync[1],
                                               z=z
                                               ).flux.value
                        )
        return np.log10(flux_density)

    # To fit all parts of the spectrum, the fit needs to be done in log
    # space, otherwise only the dominant high frequency peak will be fit.
    # For y = log10(x),  err_y = err_x / (x ln(10))

    if fit_magnification:
        p0 = (magnification, spectral_emissivity_index,
                    temperature, total_dust_mass       )
    else:
        p0 = spectral_emissivity_index, temperature, total_dust_mass


    sigma = spectrum.uncertainty.value / (spectrum.flux.value * np.log(10))

    props = {}
    try:
        popt, pcov = curve_fit(log10_one_grey_body,
                               spectrum.disp, np.log10(spectrum.flux.value),
                               p0=p0,
                               sigma=sigma,
                               )
    except:  # RuntimeError
        return props

    if fit_magnification:
        props['magnification'] = popt[0]
        start = 1
    else:
        props['magnification'] = magnification
        start = 0
    (props['spectral_emissivity_index'], props['temperature'],
        props['total_dust_mass']) = popt[start:]
    props['synchrotron_spectral_index'] = popt_sync[1]

    # Extract spectrum, properties of the fit to a dictionary
    props.update(extract_properties_from_fit(
            log10_one_grey_body,
            magnification=magnification,
            optimal_parameters=popt,
            z=z,
            output_spectrum_disp_low=disp_low,
            output_spectrum_disp_high=disp_high,
            output_spectrum_disp_step=disp_step,
            output_spectrum_num_steps=num_steps,
            ))

    return props


def fit_power_law_temp(spectrum, z, **kwargs):
    """
    Fit `spectrum` with a power-law distribution of grey bodies + synchrotron.

    The power-law is defined as:
    dM/dT = dust_mass_scalar * T ** (-dust_mass_temperature_index)
    where M is the mass of dust at temperature T emitting as a grey body.

    The synchrotron flux is given by:
    flux = synchrotron_scalar * freq ** synchrotron_spectral_index,

    Parameters
    ----------
    spectrum : astro_utils.spectra_utils.Spectrum1D or similar
        Contains ``disp``, ``flux`` astropy.nddata.NDData attributes.
    area : float
        The projected area of the source in m**2.
    z : float
        The redshift of the source.
    dust_mass_temperature_index : float, optional
        Initial guess, (default=7).
    magnification : float, optional
        Flux magnification factor from gravitational lensing, (default=``1``).
    minimum_temperature : float, optional
        Initial guess of the temperature of the coolest (dominant) component of
        the emitting dust in K, (default=``30``).
    spectral_emissivity_index : float, optional
        Often denoted as 'beta', (default=``1.8``).
    synchrotron_scalar : float, optional
        Initial guess, (default=``1e30``).
    synchrotron_spectral_index : float, optional
        Initial guess, (default=``-0.75``).
    total_dust_mass : float, optional
        Initial guess of the total mass of dust within the source in kg,
        (default=``2e39``, 1e9 * M_sun).

    Returns
    -------

    """
    print("fitting power law temp")
    # Kwargs
    area = kwargs.get('area', 4 * (u.pc.to(u.m, 1e3)) ** 2)
    fit_area = kwargs.get('fit_area', False)

    beta = kwargs.get('spectral_emissivity_index', 1.8)
    fit_beta = kwargs.get('fit_spectral_emissivity_index', False)

    gamma = kwargs.get('dust_mass_temperature_index', 7.2)  # Shorter to type
    fit_gamma = kwargs.get('fit_dust_mass_temperature_index', False)

    magnification = kwargs.get('magnification', 1)
    fit_magnification = kwargs.get('fit_magnification', False)

    # If not enough radio data, use these values
    firrc = kwargs.get('fir_radio_correlation', 2.4)
    alpha = kwargs.get('synchrotron_spectral_index', -0.75)

    # Initial guesses
    synchrotron_scalar = kwargs.get('synchrotron_scalar', 1e37)
    total_dust_mass = kwargs.get('total_dust_mass', 2e39)
    minimum_temperature = kwargs.get('minimum_temperature', 30)

    # Output spectrum
    disp_low = kwargs.get('output_spectrum_disp_low', 1e8 * u.Hz)
    disp_high = kwargs.get('output_spectrum_disp_high', 1e15 * u.Hz)
    disp_step = kwargs.get('output_spectrum_disp_step', None)
    num_steps = kwargs.get('output_spectrum_num_steps', None)

    # Possible free parameters to fit
    power_law_grey_body_params = np.array([
            area, magnification, gamma,
            beta, minimum_temperature, total_dust_mass])

    # Which of these will be fit
    power_law_grey_body_params_to_fit = np.array([
        fit_area, fit_magnification, fit_gamma, fit_beta, True, True])

    synchrotron_kwargs = copy.deepcopy(kwargs)
    synchrotron_kwargs['spectral_index'] = alpha

    # Fit the synchrotron component first
    popt_sync, pcov_sync = fit_synchrotron(spectrum, z, **synchrotron_kwargs)

    # Now fit a power law grey body + the above synchrotron spectrum.
    best_args = None

    def log10_power_law_grey_bodies(disp, *args):

        """ """
        parameters = []
        i = 0
        for index, fit_param in enumerate(power_law_grey_body_params_to_fit):
            if fit_param:
                parameters.append(args[i])
                i += 1
            else:
                parameters.append(power_law_grey_body_params[index])

        (area, magnification, gamma, beta,
                minimum_temperature, total_dust_mass) = parameters

        if beta > 2.5:
            beta = 2.5
        elif beta < 1:
            beta = 1

        flux_density = (
                observed_power_law_temp_dust_model(
                        disp,
                        area=area,
                        magnification=magnification,
                        spectral_emissivity_index=beta,
                        dust_mass_temperature_index=gamma,
                        minimum_temperature=minimum_temperature,
                        total_dust_mass=total_dust_mass,
                        z=z,
                        ).flux.value
                + observed_synchrotron(
                        disp,
                        magnification=magnification,
                        scalar=popt_sync[0],
                        spectral_index=popt_sync[1],
                        z=z
                        ).flux.value
                )

        return np.log10(flux_density)

    # To fit all parts of the spectrum, the fit needs to be done in log
    # space, otherwise only the dominant high frequency peak will be fit.
    # For y = log10(x),  err_y = err_x / (x ln(10))
    sigma = spectrum.uncertainty.value / (spectrum.flux.value * np.log(10))

    p0 = power_law_grey_body_params[power_law_grey_body_params_to_fit == True]

    try:
        popt, pcov = curve_fit(log10_power_law_grey_bodies,
                               spectrum.disp, np.log10(spectrum.flux.value),
                               p0=p0,
                               sigma=sigma,
                               absolute_sigma=True,
                               maxfev=5000,
                               ftol=1e-5,
                               #args=(power_law_grey_body_params,
                               #        power_law_grey_body_params_to_fit),
                               )
    except RuntimeError:
        print("RuntimeError")
        popt = best_args

    parameters = []
    i = 0
    for index, fit_param in enumerate(power_law_grey_body_params_to_fit):
        if fit_param:
            parameters.append(popt[i])
            i += 1
        else:
            parameters.append(power_law_grey_body_params[index])

    props = {
        'area': parameters[0],
        'area_mag': parameters[0] * parameters[1],
        'magnification': parameters[1],
        'dust_mass_temperature_index': parameters[2],
        'spectral_emissivity_index': parameters[3],
        'minimum_temperature': parameters[4],
        'total_dust_mass': parameters[5],
        'total_dust_mass_mag': parameters[5] * parameters[1],
        'synchrotron_spectral_index': popt_sync[1],
        }

    # Extract spectrum, properties of the fit to a dictionary
    props.update(extract_properties_from_fit(
            log10_power_law_grey_bodies,
            magnification=props['magnification'],
            optimal_parameters=popt,
            z=z,
            output_spectrum_disp_low=disp_low,
            output_spectrum_disp_high=disp_high,
            output_spectrum_disp_step=disp_step,
            output_spectrum_num_steps=num_steps,
            ))


    def fit_synchrotron_scalar(scalar_value):
        if scalar_value < 1e27:
            return 1000000
        popt_sync[0] = scalar_value
        p = extract_properties_from_fit(
                        log10_power_law_grey_bodies,
                        magnification=props['magnification'],
                        optimal_parameters=popt,
                        z=z,
                        output_spectrum_disp_low=disp_low,
                        output_spectrum_disp_high=disp_high,
                        output_spectrum_disp_step=disp_step,
                        output_spectrum_num_steps=num_steps,
                        )
        return abs(p['fir_radio_correlation'] - firrc)
        #return abs(extract_properties_from_fit(
        #                log10_power_law_grey_bodies,
        #                magnification=props['magnification'],
        #                optimal_parameters=popt,
        #                z=z,
        #                output_spectrum_disp_low=disp_low,
        #                output_spectrum_disp_high=disp_high,
        #                output_spectrum_disp_step=disp_step,
        #                output_spectrum_num_steps=num_steps,
        #                )['fir_radio_correlation'] - firrc)

    # If there wasn't a radio point, use firrc to set synchrotron sed
    if popt_sync[0] < 1:
        import scipy.optimize
        popt_sync[0] = scipy.optimize.minimize_scalar(fit_synchrotron_scalar,
                bracket=(1e29, 1e31), tol=0.01).x
        props.update(extract_properties_from_fit(
                        log10_power_law_grey_bodies,
                        magnification=props['magnification'],
                        optimal_parameters=popt,
                        z=z,
                        output_spectrum_disp_low=disp_low,
                        output_spectrum_disp_high=disp_high,
                        output_spectrum_disp_step=disp_step,
                        output_spectrum_num_steps=num_steps,
                        ))

    return props


def fit_sed(spectrum, **fit_kwargs):
    """
    Each fitting routine returns the best fit to the input data.
    maths_utils.uncertainty can be used to find the uncertainty on the returned
    parameters, however it needs to accept and return a list/array rather
    than a dictionary, so a new function needs to be created
    """

    # Fit a single temperature component
    def fit_one_temp_cov(flux_magnification, disp, uncertainty, **kwargs):
        """ """
        if kwargs.get('fit_magnification', False): # use kwargs value as guess
            flux = flux_magnification
        else:   # get given it instead
            flux = flux_magnification[:-1]
            kwargs['magnification'] = flux_magnification[-1]

        # fit_one_temp takes a spec_utils.Spectrum1D, so create
        spec = Spectrum1D(disp=disp * u.Hz,
                          flux=flux * u.W / u.m ** 2 / u.Hz,
                          uncertainty=uncertainty * u.W / u.m ** 2 / u.Hz
                          )

        one = fit_one_temp(spec, **kwargs)

        # one is now a dictionary
        return [one['bolometric_luminosity'],
                one['fir_luminosity'],
                one['ir_luminosity'],
                one['40-500_luminosity'],
                one['fir_radio_correlation'],
                one['magnification'],
                one['spectral_emissivity_index'],
                one['synchrotron_spectral_index'],
                one['temperature'],
                one['total_dust_mass'],
                ]

    ## Fit two temperature components
    #def fit_two_temp_cov(flux, disp, uncertainty, **kwargs):
    #    """ """
    #    # fit_two_temp takes a spec_utils.Spectrum1D, so create
    #    spec = Spectrum1D(disp=NDData(disp, unit=u.Hz),
    #                      flux=NDData(flux, unit=(u.W / u.m ** 2 / u.Hz)),
    #                      uncertainty=NDData(uncertainty,
    #                                         unit=(u.W / u.m ** 2 / u.Hz)))

    #    two = fit_two_temp(spectrum, **kwargs)

    # Fit power-law temperature components
    def fit_power_law_temp_cov(flux_parameters, disp, uncertainty, **kwargs):
        """ """

        # Extract varied parameters off the end of the fluxes
        if not power_law_kwargs['fit_synchrotron_spectral_index']:
            kwargs['synchrotron_spectral_index'] = flux_parameters[-1]
            flux_parameters = flux_parameters[:-1]
        if not power_law_kwargs['fit_spectral_emissivity_index']:
            kwargs['spectral_emissivity_index'] = flux_parameters[-1]
            flux_parameters = flux_parameters[:-1]
        if not power_law_kwargs['fit_dust_mass_temperature_index']:
            kwargs['dust_mass_temperature_index'] = flux_parameters[-1]
            flux_parameters = flux_parameters[:-1]
        if not power_law_kwargs['fit_area']:
            kwargs['area'] = flux_parameters[-1]
            flux_parameters = flux_parameters[:-1]
        if not power_law_kwargs.get('fit_magnification', False):
            kwargs['magnification'] = flux_parameters[-1]
            flux_parameters = flux_parameters[:-1]

        # flux_parameters now only contains fluxes
        flux = flux_parameters

        # fit_power_law_temp takes a spec_utils.Spectrum1D
        spec = Spectrum1D(disp=disp * u.Hz,
                          flux=flux * u.W / u.m ** 2 / u.Hz,
                          uncertainty=uncertainty * u.W / u.m ** 2 / u.Hz)

        power_law = fit_power_law_temp(spec, **kwargs)
        # power_law is now a dictionary
        return [power_law['area'],
                power_law['area_mag'],
                power_law['bolometric_luminosity'],
                power_law['bolometric_luminosity_mag'],
                power_law['dust_mass_temperature_index'],
                power_law['fir_luminosity'],
                power_law['fir_luminosity_mag'],
                power_law['fir_radio_correlation'],
                power_law['ir_luminosity'],
                power_law['ir_luminosity_mag'],
                power_law['40-500_luminosity'],
                power_law['40-500_luminosity_mag'],
                power_law['60_luminosity_density'],
                power_law['60_luminosity_density_mag'],
                power_law['100_luminosity_density'],
                power_law['100_luminosity_density_mag'],
                power_law['magnification'],
                power_law['minimum_temperature'],
                power_law['spectral_emissivity_index'],
                power_law['synchrotron_spectral_index'],
                power_law['total_dust_mass'],
                power_law['total_dust_mass_mag'],
                ]

    # First convert spectrum units
    spectrum.disp = mathsu.to_unit(u.Hz, spectrum.disp)
    spectrum.flux = mathsu.to_unit(u.W / u.m ** 2 / u.Hz, spectrum.flux)
    spectrum.uncertainty = mathsu.to_unit(u.W / u.m ** 2 / u.Hz, spectrum.uncertainty)

    # Perform the various fits
    raw_fits = {}

    ## fit a single temperature component using pre-found magnification
    #one_kwargs = fit_kwargs.copy()
    #one_kwargs['fit_magnification'] = False

    #raw_fits['one'] = fit_one_temp(spectrum, **one_kwargs)

    #one_cov = mathsu.covariance(
    #        fit_one_temp_cov,  # function
    #        # flux, mag
    #        [np.hstack((spectrum.flux.data,
    #                    one_kwargs['magnification']))],
    #        # flux, mag uncertainty
    #        np.diag(np.hstack((spectrum.uncertainty.data ** 2,
    #                           one_kwargs['magnification_uncertainty'] ** 2))),
    #        # Other args, kwargs for function
    #        args=[spectrum.disp.data, spectrum.uncertainty.data],
    #        kwargs=one_kwargs,
    #        )
    #one_covariance = {}
    #(one_covariance['bolometric_luminosity'],
    #        one_covariance['fir_luminosity'],
    #        one_covariance['ir_luminosity'],
    #        one_covariance['40-500_luminosity'],
    #        one_covariance['fir_radio_correlation'],
    #        one_covariance['magnification'],
    #        one_covariance['spectral_emissivity_index'],
    #        one_covariance['synchrotron_spectral_index'],
    #        one_covariance['temperature'],
    #        one_covariance['total_dust_mass']
    #        ) = np.sqrt(np.diag(one_cov))
    #raw_fits['one_covariance'] = one_covariance

    ## Fit a single temperature component and fit magnification
    #one_mag_kwargs = fit_kwargs.copy()
    #one_mag_kwargs['fit_magnification'] = True

    #raw_fits['one_magnification'] = fit_one_temp(spectrum, **one_mag_kwargs)

    #one_mag_cov = mathsu.covariance(
    #        fit_one_temp_cov,
    #        [spectrum.flux.data],
    #        np.diag(spectrum.uncertainty.data ** 2),
    #        args=[spectrum.disp.data, spectrum.uncertainty.data],
    #        kwargs=one_mag_kwargs,
    #        )
    #one_magnification_covariance = {}
    #(one_magnification_covariance['bolometric_luminosity'],
    #        one_magnification_covariance['fir_luminosity'],
    #        one_magnification_covariance['ir_luminosity'],
    #        one_magnification_covariance['40-500_luminosity'],
    #        one_magnification_covariance['fir_radio_correlation'],
    #        one_magnification_covariance['magnification'],
    #        one_magnification_covariance['spectral_emissivity_index'],
    #        one_magnification_covariance['synchrotron_spectral_index'],
    #        one_magnification_covariance['temperature'],
    #        one_magnification_covariance['total_dust_mass']
    #        ) = np.sqrt(np.diag(one_mag_cov))
    #raw_fits['one_magnification_covariance'] = one_magnification_covariance

    #two = fit_two_temp(spectrum, **fit_kwargs)
    #two_uncertainty = mathsu.uncertainty(fit_two_temp_cov,
    #                         [spectrum.flux.data],
    #                         [spectrum.uncertainty.data],
    #                         args=[spectrum.disp.data, spectrum.uncertainty.data],
    #                         kwargs=fit_kwargs,
    #                         )

    # Fit a power law temperature distribution using pre-found magnification
    power_law_kwargs = fit_kwargs.copy()

    raw_fits['power_law'] = fit_power_law_temp(spectrum, **power_law_kwargs)

    # Determine parameters to vary in covariance method
    values = (spectrum.flux.value, )
    variances = (spectrum.uncertainty.value ** 2, )
    if not power_law_kwargs.get('fit_magnification', False):
        values += (power_law_kwargs.get('magnification', 1), )
        variances += (power_law_kwargs.get('magnification_uncertainty', 0) ** 2, )
    if not power_law_kwargs['fit_area']:
        values += (power_law_kwargs['area'], )
        variances += (power_law_kwargs['area_uncertainty'] ** 2, )
    if not power_law_kwargs['fit_dust_mass_temperature_index']:
        values += (power_law_kwargs['dust_mass_temperature_index'], )
        variances += (power_law_kwargs[
            'dust_mass_temperature_index_uncertainty'] ** 2, )
    if not power_law_kwargs['fit_spectral_emissivity_index']:
        values += (power_law_kwargs['spectral_emissivity_index'], )
        variances += (power_law_kwargs[
            'spectral_emissivity_index_uncertainty'] ** 2, )

    radio_min = redshift(4 * u.mm, power_law_kwargs['z'], disp_type='wavelength')
    radio_max = 1 * u.Hz
    radio_spectrum = spectrum.slice_dispersion(radio_min, radio_max)
    radio_spectrum.sort_disp()
    if (len(radio_spectrum) < 2  # not enough points to fit synchrotron index
            or not power_law_kwargs['fit_synchrotron_spectral_index']):
        power_law_kwargs['fit_synchrotron_spectral_index'] = False
        values += (power_law_kwargs['synchrotron_spectral_index'], )
        variances += (power_law_kwargs[
            'synchrotron_spectral_index_uncertainty'] ** 2, )

    # Fluxes and magnification are inputs to find variance of output parameters
    print("fitting in covariance")
    power_law_cov = mathsu.covariance(
            fit_power_law_temp_cov,
            [np.hstack(values)],
            np.diag(np.hstack(variances)),
            args=[spectrum.disp.value, spectrum.uncertainty.value],
            kwargs=power_law_kwargs,
            )
    power_law_covariance = {}
    (power_law_covariance['area'],
            power_law_covariance['area_mag'],
            power_law_covariance['bolometric_luminosity'],
            power_law_covariance['bolometric_luminosity_mag'],
            power_law_covariance['dust_mass_temperature_index'],
            power_law_covariance['fir_luminosity'],
            power_law_covariance['fir_luminosity_mag'],
            power_law_covariance['fir_radio_correlation'],
            power_law_covariance['ir_luminosity'],
            power_law_covariance['ir_luminosity_mag'],
            power_law_covariance['40-500_luminosity'],
            power_law_covariance['40-500_luminosity_mag'],
            power_law_covariance['60_luminosity_density'],
            power_law_covariance['60_luminosity_density_mag'],
            power_law_covariance['100_luminosity_density'],
            power_law_covariance['100_luminosity_density_mag'],
            power_law_covariance['magnification'],
            power_law_covariance['minimum_temperature'],
            power_law_covariance['spectral_emissivity_index'],
            power_law_covariance['synchrotron_spectral_index'],
            power_law_covariance['total_dust_mass'],
            power_law_covariance['total_dust_mass_mag'],
            ) = np.sqrt(np.diag(power_law_cov))
    raw_fits['power_law_covariance'] = power_law_covariance

    ## Fit a power law temperature distribution also fitting magnification
    #power_law_mag_kwargs = fit_kwargs.copy()
    #power_law_mag_kwargs['fit_magnification'] = True

    #raw_fits['power_law_magnification'] = fit_power_law_temp(
    #        spectrum, **power_law_mag_kwargs)

    #print("getting covariance")
    #if power_law_mag_kwargs['fixed_area']:
    #    power_law_mag_cov = mathsu.covariance(
    #            fit_power_law_temp_cov,
    #            [spectrum.flux.value],
    #            np.diag(spectrum.uncertainty.value ** 2),
    #            args=[spectrum.disp.value, spectrum.uncertainty.value],
    #            kwargs=power_law_mag_kwargs,
    #            )
    #else:
    #    power_law_mag_cov = mathsu.covariance(
    #            fit_power_law_temp_cov,
    #            [np.hstack((spectrum.flux.value,
    #                        power_law_mag_kwargs['area']
    #                        ))],
    #            np.diag(np.hstack(
    #                (spectrum.uncertainty.value ** 2,
    #                 power_law_mag_kwargs['area_uncertainty'] ** 2,
    #                 ))),
    #            args=[spectrum.disp.value, spectrum.uncertainty.value],
    #            kwargs=power_law_mag_kwargs,
    #            )
    #print("got covariance")
    #power_law_magnification_covariance = {}
    #(power_law_magnification_covariance['area'],
    #        power_law_magnification_covariance['area_mag'],
    #        power_law_magnification_covariance['bolometric_luminosity'],
    #        power_law_magnification_covariance['bolometric_luminosity_mag'],
    #        power_law_magnification_covariance['dust_mass_temperature_index'],
    #        power_law_magnification_covariance['fir_luminosity'],
    #        power_law_magnification_covariance['fir_luminosity_mag'],
    #        power_law_magnification_covariance['fir_radio_correlation'],
    #        power_law_magnification_covariance['ir_luminosity'],
    #        power_law_magnification_covariance['ir_luminosity_mag'],
    #        power_law_magnification_covariance['40-500_luminosity'],
    #        power_law_magnification_covariance['40-500_luminosity_mag'],
    #        power_law_magnification_covariance['magnification'],
    #        power_law_magnification_covariance['minimum_temperature'],
    #        power_law_magnification_covariance['spectral_emissivity_index'],
    #        power_law_magnification_covariance['synchrotron_spectral_index'],
    #        power_law_magnification_covariance['total_dust_mass'],
    #        power_law_magnification_covariance['total_dust_mass_mag'],
    #        ) = np.sqrt(np.diag(power_law_mag_cov))
    #raw_fits['power_law_magnification_covariance'] = (
    #        power_law_magnification_covariance)

    # fill fits dictionary
    fits = {}
    #for model_type in 'one', 'power_law':
    for model_type in ('power_law', ):  # ('one', 'power_law')
        for mag in ('', ) :  # ('', '_magnification')
            fits[model_type + mag] = {
                    key: (value,
                          raw_fits[model_type + mag + '_covariance'].get(key,
                                                                         None)
                          )
                    for key, value in raw_fits[model_type + mag].items()
                    }

    return fits


def fit_synchrotron(spectrum, z, **kwargs):
    """
    """
    magnification = kwargs.get('magnification', 1)
    scalar = kwargs.get('scalar', 1e30)
    spectral_index = kwargs.get('spectral_index', -0.75)
    fit_spectral_index = kwargs.get('fit_synchrotron_spectral_index', False)

    # Fit in log space to cover the large o.o.m. range
    # Create a new function accepting only the fitting parameters
    def log10_synchrotron(disp, *args):
        nonlocal spectral_index
        if fit_spectral_index:
            try:
                scalar, spec_index = args
            except ValueError:  # only given scalar anyway
                spec_index = copy.copy(spectral_index)
                scalar = args
        else:
            spec_index = copy.copy(spectral_index)
            scalar = args
        return np.log10(observed_synchrotron(disp,
                                             magnification=magnification,
                                             scalar=scalar,
                                             spectral_index=spec_index,
                                             z=z,
                                             ).flux.value)
                                             #).flux.data[0])

    # Take just the radio photometry points to fit to
    # Cut at 4mm in rest frame
    radio_min = redshift(4 * u.mm, z, disp_type='wavelength')
    radio_max = 1 * u.Hz
    radio_spectrum = spectrum.slice_dispersion(radio_min, radio_max)
    radio_spectrum.sort_disp()
    #sigma = (radio_spectrum.uncertainty.data
    #         / (radio_spectrum.flux.data * np.log(10)))
    sigma = (radio_spectrum.uncertainty.value
             / (radio_spectrum.flux.value * np.log(10)))

    if len(radio_spectrum) == 0:  # no radio points - can't fit synchrotron
        popt_sync = np.array([0, spectral_index])
        pcov_sync = np.array([[0, 0], [0, 0]])

    elif len(radio_spectrum) == 1:
        # can't fit spectral index
        def log10_synchrotron_scalar(sc):
            return np.abs(
                log10_synchrotron(radio_spectrum.disp, 10 ** sc)
                    - np.log10(radio_spectrum.flux.value)
                )

        popt_sync = minimize(log10_synchrotron_scalar,
                             np.log10(scalar),
                             ).x
        popt_sync = np.array([10 ** popt_sync[0], spectral_index])
        pcov_sync = np.array([[0, 0], [0, 0]])

    else:  # can do a proper fit
        if fit_spectral_index:
            p0 = (scalar, spectral_index)
        else:
            p0 = scalar

        # Perform the fit
        # For y = log10(x),  err_y = err_x / (x ln(10))
        popt_sync, pcov_sync = curve_fit(log10_synchrotron,
                                         radio_spectrum.disp,
                                         np.log10(radio_spectrum.flux.value),
                                         p0=p0,
                                         sigma=sigma,
                                         maxfev=2000
                                         )
        if not fit_spectral_index:
            popt_sync = np.array([popt_sync[0], spectral_index])
            pcov_sync = np.array([[pcov_sync[0, 0], 0], [0, 0]])

    return popt_sync, pcov_sync


def fit_two_temp(spectrum, area, z, **kwargs):
    """
    Fit two grey bodies and synchrotron emission to `spectrum`.

    The synchrotron flux is given by:
    flux = synchrotron_scalar * freq ** (-synchrotron_spectral_index),

    Parameters
    ----------
    spectrum : astro_utils.spec_utils.Spectrum1D or similar
        Contains ``disp``, ``flux`` astropy.nddata.NDData attributes.
    area : float
        The projected area of the source in m**2.
    z : float
        The redshift of the source.
    dust_mass_fraction_hotter : float, optional
        Initial guess of the fraction of total_dust_mass within the hotter dust
        component, (default=``0.01``).
    magnification : float, optional
        Flux magnification factor from gravitational lensing, (default=``1``).
    spectral_emissivity_index : float, optional
        Often denoted as 'beta', (default=``1.8``).
    synchrotron_scalar : float, optional
        Initial guess, (default=``1e30``).
    synchrotron_spectral_index : float, optional
        Initial guess, (default=``0.75``).
    temperature_cooler : float, optional
        Initial guess of the temperature cooler dust component in K,
        (default=``30``).
    temperature_hotter : float, optional
        Initial guess of the temperature hotter dust component in K,
        (default=``60``).
    total_dust_mass : float, optional
        Initial guess of the total mass of dust within the source in kg,
        (default=``2e39``, 1e9 * M_sun).

    Returns
    -------

    """
    # Initial guesses
    frac_hot = kwargs.get('dust_mass_fraction_hotter', 0.01)
    magnification = kwargs.get('magnification', 1)
    magnification_uncertainty = kwargs.get('magnification_uncertainty', 0)
    disp_low = kwargs.get('output_spectrum_disp_low', 1e8 * u.Hz)
    disp_high = kwargs.get('output_spectrum_disp_high', 1e15 * u.Hz)
    disp_step = kwargs.get('output_spectrum_disp_step', None)
    num_steps = kwargs.get('output_spectrum_num_steps', None)
    spectral_emissivity_index = kwargs.get('spectral_emissivity_index', 1.8)
    synchrotron_scalar = kwargs.get('synchrotron_scalar', 1e37)
    synchrotron_spectral_index = kwargs.get('synchrotron_spectral_index', -0.75)
    temp_cool = kwargs.get('temperature_cooler', 30)
    temp_hot = kwargs.get('temperature_hotter', 60)
    total_dust_mass = kwargs.get('total_dust_mass', 2e39)

    # Fit the synchrotron component first
    popt_sync, pcov_sync = fit_synchrotron(spectrum, z, **kwargs)

    # Now fit 2 grey bodies + the previously calculated synchrotron spectrum.
    # Create a new function accepting only the fitting parameters
    def log10_two_grey_bodies(disp, frac_hot, temp_cool,
                              temp_hot, total_dust_mass ):
        """ A double grey body + synchrotron spectrum to fit this source. """
        if frac_hot > 1:
            return 1e100
        # Luminosity density spectrum
        spec = (observed_grey_body(disp,
                                   area=area,
                                   dust_mass_fraction=(1 - frac_hot, frac_hot),
                                   magnification=magnification,
                                   spectral_emissivity_index=\
                                       spectral_emissivity_index,
                                   temperature=(temp_cool, temp_hot),
                                   total_dust_mass=total_dust_mass,
                                   z=z
                                   ).flux.value
                + observed_synchrotron(disp,
                                       magnification=magnification,
                                       scalar=popt_sync[0],
                                       spectral_index=popt_sync[1],
                                       z=z
                                       ).flux.value
                )
        return np.log10(spec)

    # To fit all parts of the spectrum, the fit needs to be done in log
    # space, otherwise only the dominant high frequency peak will be fit.
    # For y = log10(x),  err_y = err_x / (x ln(10))
    sigma = spectrum.uncertainty.value / (spectrum.flux.value * np.log(10))
    popt, pcov = curve_fit(log10_two_grey_bodies,
                           spectrum.disp, np.log10(spectrum.flux.value),
                           p0=(frac_hot, temp_cool, temp_hot, total_dust_mass),
                           sigma=sigma
                           )

    # Extract spectrum, properties of the fit to a dictionary
    props = extract_properties_from_fit(log10_two_grey_bodies,
                                        magnification=magnification,
                                        optimal_parameters=popt,
                                        z=z,
                                        output_spectrum_disp_low=disp_low,
                                        output_spectrum_disp_high=disp_high,
                                        output_spectrum_disp_step=disp_step,
                                        output_spectrum_num_steps=num_steps,
                                        )

    props['dust_mass_fraction_hotter'] = popt[0]
    props['minimum_temperature'] = popt[2]
    props['spectral_emissivity_index'] = popt[3]
    props['synchrotron_spectral_index'] = popt_sync[1]
    props['temperature_cooler'] = popt[1]
    props['temperature_hotter'] = popt[2]
    props['total_dust_mass'] = popt[3]
    return props
