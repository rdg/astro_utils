"""
This module contains useful functions that deal with adding spectra,
baseline subtraction, unit conversions, fitting with grey bodies etc.
"""
from __future__ import division, print_function
import copy
import os
import sys
import warnings
warnings.simplefilter('ignore', RuntimeWarning)

import astropy
from astropy.cosmology import Planck13 as cosmology
#from astropy.cosmology import FlatLambdaCDM
#cosmology = FlatLambdaCDM(H0=71, Om0=0.27)
from astropy.io import fits
import astropy.units as u
#from astropy.specutils import Spectrum1D
import numpy as np
import astropy.constants.si as con
from scipy.interpolate import UnivariateSpline
from scipy.optimize import curve_fit

from astro_utils import moving
import astro_utils.maths_utils as mathsu


class Spectrum1D(object):
    """ This is a filler for the Spectrum1D class in astropy
    """
    def __init__(self, disp=None, flux=None, uncertainty=None):
        """ A 1d spectrum class filling in for astropy

        Parameters
        ---------
        disp : astropy.nddata.NDData
            The frequency or wavelength etc of the spectrum
        flux : astropy.nddata.NDData
            The flux of the spectrum
        uncertainty : astropy.nddata.NDData
            The uncertainty of the flux
        """
        # Should put some type checks in for dispersion and flux

        self.size = None

        self.disp = disp
        #if disp is None:
        #    self.disp = NDData(None)

        self.flux = flux
        if flux is None:
            pass
            #self.flux = NDData(None)
        else:
            try:
                self.size = len(flux)
            except TypeError:  # Unsized array (single point)
                self.size = 1

        self._uncertainty = uncertainty
        if uncertainty is None:
            if flux is not None and flux.value.ndim == 1 and self.size > 1:
                #self._uncertainty = NDData(
                #        np.sqrt(mathsu.variance_at_pos(self.flux.value)),
                #        unit=self.flux.unit)
                self._uncertainty = (
                        np.sqrt(mathsu.variance_at_pos(self.flux.value))
                            * self.flux.unit)
            #else:
            #    self._uncertainty = NDData(None)

    def __len__(self):
        return len(self.disp)

    def copy(self):
        """
        Return a copy of this Spectrum1D
        """
        return copy.deepcopy(self)

    def slice_dispersion(self, start, stop, velocity_zero_point=None):
        """Slice the spectrum within a given start and end dispersion value.

        Parameters
        ----------
        start : `float`
            Starting slice point.
        stop : `float`
            Stopping slice point.

        Returns
        -------
        Spectrum1D :
            The sliced spectrum

        Notes
        -----
        Often it is useful to slice out a portion of a `Spectrum1D` objects
        either by two dispersion points (e.g. two wavelengths) or by the
        indices of the dispersion/flux arrays (see `~Spectrum1D.slice_index`
        for this functionality).
        """
        try:
            if start.unit is not self.disp.unit:
                begin = mathsu.to_unit(self.disp.unit,
                                       start,
                                       velocity_zero_point
                                       ).value
            else:
                begin = start.value
        except AttributeError:
            begin = start

        try:
            if stop.unit is not self.disp.unit:
                end = mathsu.to_unit(self.disp.unit,
                                     stop,
                                     velocity_zero_point
                                     ).value
            else:
                end = stop.value
        except AttributeError:
            end = stop

        if begin is None:
            begin = 1.5 * self.disp.value[0] - 0.5 * self.disp.value[1]
        if end is None:
            end = 1.5 * self.disp.value[-1] - 0.5 * self.disp.value[-2]

        # Transform the dispersion end points to index space
        if self.disp.value[0] < self.disp.value[1]:  # ascending order
            start_index, stop_index = \
                self.disp.searchsorted([begin, end] * self.disp.unit)
                #self.disp.data.searchsorted([begin, end])
        else:  # descending order
            start_index, stop_index = \
                (len(self) - self.disp[::-1].searchsorted([begin, end] * self.disp.unit))
                #(len(self) - self.disp.data[::-1].searchsorted([begin, end]))

        return self.slice_index(start_index, stop_index)


    def slice_index(self, start, stop):
        """Slice the spectrum within a given start and end index.

        Parameters
        ----------
        start : `float`
            Starting slice point.
        stop : `float`
            Stopping slice point.

        Notes
        -----
        Often it is useful to slice out a portion of a `Spectrum1D` objects
        either by two index points (see `~Spectrum1D.slice_dispersion`) or by
        the indices of the dispersion/flux array.

        See Also
        --------
        `~Spectrum1D.slice_dispersion`
        """

        # We need to slice the following items:
        # >> disp, flux, uncertainty, mask, and flags
        # Which are all common NDData objects, therefore I am (perhaps
        # reasonably) assuming that __slice__ will be a NDData base function
        # which we will inherit.
        sliced_spec = Spectrum1D()
        start = max(start, 0)
        spec_slice = slice(min(start, stop), max(start, stop))
        self_copy = self.copy()
        #sliced_spec.disp = NDData(self_copy.disp.data[spec_slice],
        #                          unit=self_copy.disp.unit      )
        #sliced_spec.flux = NDData(self_copy.flux.data[spec_slice],
        #                          unit=self_copy.flux.unit      )
        sliced_spec.disp = self_copy.disp[spec_slice]
        sliced_spec.flux = self_copy.flux[
                [slice(None)] * (len(self.flux.shape) - 1) + [spec_slice]]
        try:
            #sliced_spec.uncertainty = NDData(self_copy.uncertainty.data[spec_slice],
            #                                 unit=self_copy.uncertainty.unit      )
            sliced_spec.uncertainty = self_copy.uncertainty[spec_slice]
        except AttributeError:  # self has no uncertainty
            pass
        #sliced_spec.size = sliced_spec.disp.data.size
        sliced_spec.size = sliced_spec.disp.value.size
        if start > stop:
            #sliced_spec.disp.data = sliced_spec.disp.data[::-1]
            #sliced_spec.flux.data = sliced_spec.flux.data[::-1]
            sliced_spec.disp = sliced_spec.disp[::-1]
            sliced_spec.flux = sliced_spec.flux[
                [slice(None)] * (len(self.flux.shape) - 1) + [slice(None, None, -1)]]
            try:
                #sliced_spec.uncertainty.data = sliced_spec.uncertainty.data[::-1]
                sliced_spec.uncertainty = sliced_spec.uncertainty[::-1]
            except AttributeError:
                pass
        return sliced_spec

    @property
    def uncertainty(self):
        try:
            #if self._uncertainty.data == np.array(None):
            #    self._uncertainty.data = mathsu.variance_at_pos(self.flux)
            #    self._uncertainty.unit = self.flux.unit
            if self._uncertainty.value == np.array(None):
                self._uncertainty = mathsu.variance_at_pos(self.flux) * self.flux.unit
        except ValueError:
            pass
        #if self._uncertainty is None:
        #    self._uncertainty = NDData(mathsu.variance_at_pos(self.flux),
        #                               unit=self.flux.unit)
        return self._uncertainty

    @uncertainty.setter
    def uncertainty(self, value):
        #if type(value) is NDData:
            self._uncertainty = value
        #else:
            #self._uncertainty.data = value


class Target(object):
    """
    A container for all information on a target: name, coords, redshift etc
    """
    def __init__(self, name=None, spectrum=None, redshift=None):
        self.name = name
        self.spectrum = spectrum
        self.redshift = redshift
        self.right_ascension = None
        self.declination = None

    def __len__(self):
        return len(self.spectrum)


def add_spectra(spectrum_list, weighting='uniform', **kwargs):
    """
    Coadd an input list of spectra with optional weighting and smoothing.

    Parameters
    ----------
    spectrum_list : list of Spectrum1D objects
        The spectra to coadd
    weighting : string, optional
        The form of weighting to use, options are:
        ``uniform`` : The flux at the dispersion value in each input spectrum
                      is uniformly weighted (default);
        ``inv_var`` : The flux value in each input spectrum is weighted by the
                      inverse of the variance in a surrounding dispersion range
                      (flux equally weighted across range);
        ``median``  : The median flux value at each output dispersion value is
                      taken.
    window : str, optional
        Window function to use for ``weighting='inv_var'``, see
        ``scipy.signal`` for available functions, (default = ``'boxcar'``).
    size : int
        Size of window in number of elements of each dispersion array.
    window_args : tuple, optional
        Any arguments required for the window function e.g. sigma for
        ``window='gaussian'``, (default = ``()``).

    Returns
    -------
    Spectrum1D
        The total spectrum
    """
    # Get kwargs
    window = kwargs.pop('window', 'boxcar')
    size = kwargs.pop('size', 100)
    use_uncertainty = kwargs.pop('use_uncertainty', False)
    window_args = kwargs.pop('window_args', ())
    spectrum_list = list(spectrum_list)

    # Each spectrum is fitted with an interpolating spline and sampled at the
    # output dispersion values

    # Create empty dispersion and flux arrays, assuming all dispersion arrays
    # have the same units
    # Total spectrum bin width = mean bin width across spectra
    # Could probably generalise this to dispersion dependent widths but meh
    bin_width = np.mean(np.concatenate([(spec.disp.value[1:] -
                                         spec.disp.value[:-1] )
                                        for spec in spectrum_list]))
    disp_min = min([np.min(spec.disp.value) for spec in spectrum_list])
    disp_max = max([np.max(spec.disp.value) for spec in spectrum_list])
    disp = np.array([]) * spectrum_list[0].disp.unit

    # If Dispersion increases along array
    if spectrum_list[0].disp.value[0] < spectrum_list[0].disp.value[-1]:
        disp = np.arange(disp_min, disp_max, bin_width) * disp.unit
    else:  # Dispersion decreases along array
        disp = np.arange(disp_max, disp_min, -bin_width) * disp.unit

    # Create empty flux, uncertainty arrays for final spectrum
    flux = np.zeros(disp.value.size) * spectrum_list[0].flux.unit
    uncertainty = np.zeros(disp.value.size) * spectrum_list[0].flux.unit

    #import matplotlib.pyplot as plt
    #fig = plt.figure()
    #ax = fig.add_subplot(111)
    #for index, spectrum in enumerate(spectrum_list):
    #    ax.plot(spectrum.disp.value, spectrum.flux.value)
    #plt.show()
    #fig.clf()

    # Create array containing interpolated weight, flux, uncertainty arrays
    interpolated_spec = np.zeros((len(spectrum_list), 3, disp.size))
    for index, spectrum in enumerate(spectrum_list):

        # Indices of disp covered by spectrum_list[spec]
        disp_range = np.where(
                (disp.value >= (np.min(spectrum.disp.value) - bin_width / 10000)) &
                (disp.value <= (np.max(spectrum.disp.value) + bin_width / 10000))
                )[0]

        # Fill the relevant indices of interpolated_spec with interpolated flux
        interpolated_spec[index, 1, disp_range] = (
              UnivariateSpline(spectrum.disp.value, spectrum.flux.value, s=0)
              )(disp.value[disp_range])

        # Similarly for uncertainty
        interpolated_spec[index, 2, disp_range] = (
              UnivariateSpline(spectrum.disp.value,
                               spectrum.uncertainty.value, s=0)
              )(disp.value[disp_range])

        if weighting is 'inv_var':  # Need to get weight from variance
            # Could also use provided std dev (spectrum.uncertainty.value),
            # but this is more general and a good approximation
            if use_uncertainty:
                spectrum.weight = 1 / (spectrum.uncertainty.value ** 2)
            else:
                spectrum.weight = (
                        1 / mathsu.variance_at_pos(spectrum.flux.value,
                                                   size=size,
                                                   window=window,
                                                   window_args=window_args
                                                   ))

            # Fill the relevant indices of interpolated_spec with interp weight
            interpolated_spec[index, 0, disp_range] = (
                UnivariateSpline(spectrum.disp.value, spectrum.weight, s=0)
                )(disp.value[disp_range])
        else:  # Weights are all 1
            interpolated_spec[index, 0, disp_range] = 1

    # Different weighting methods have different coaddition methods
    # <x> = (sum w_i x_i) / (sum w_i)
    if weighting in ('uniform', 'inv_var'):
        flux = (np.sum(interpolated_spec[:, 0]
                       * interpolated_spec[:, 1], axis=0)
                / np.sum(interpolated_spec[:, 0], axis=0)
                ) * flux.unit
        uncertainty = (np.sqrt(
                        np.sum(interpolated_spec[:, 0] ** 2
                               * interpolated_spec[:, 2] ** 2, axis=0)
                        )
                / np.sum(interpolated_spec[:, 0], axis=0)
                ) * uncertainty.unit
    elif weighting is 'median':
        flux = np.ma.median(np.ma.masked_array(
            interpolated_spec[:,1], mask=(interpolated_spec[:,0] == 0.)
            ),
            axis=0) * flux.unit

    return Spectrum1D(disp, flux, uncertainty)


def array(dispersion, observed=False):
    """
    Return the SPIRE arrays covering `dispersion`
    """
    um_value = mathsu.to_unit(u.um, dispersion)
    arrays = []
    if 194.17475728 * u.um < um_value < 312.5 * u.um:
        arrays.append('SSW')
    if 303.03030303 * u.um < um_value < 670.69081153 * u.um:
        arrays.append('SLW')
    return tuple(arrays)


def baseline_subtract(spectrum):
    """
    Subtract a smoothed baseline curve from a spectrum

    Parameters
    ----------
    spectrum : Spectrum1D
        An object containing 2 arrays:
            spectrum.disp: Frequency, spectrum.flux: Flux

    Returns
    -------
    Spectrum1D
        The same as spectrum
    """
    bs_spectrum = spectrum.copy()
    bs_spectrum.baseline_subtract()
    return bs_spectrum


def baseline_subtract_spectrum(self):
    """
    Subtract the moving median from the flux
    """
    baseline = moving.moving_median(self.flux.value, size=(self.size // 6))
    #var_array = variance_at_pos(self.flux,
    #                            size=(self.size // 6), window='hanning')
    #baseline = UnivariateSpline(self.disp, self.flux,
    #                            w=(1 / np.sqrt(var_array)),
    #                            s=self.size                )(self.disp)

    #import matplotlib.pyplot as plt
    #plt.plot(self.disp.data, self.flux.data)
    #plt.plot(self.disp.data, baseline.data)

    # Subtract the baseline to set the spectrum mean to zero
    self.flux = (self.flux.value - baseline) * self.flux.unit

    #plt.plot(self.disp.data, self.flux.data)
    #plt.show()

Spectrum1D.baseline_subtract = baseline_subtract_spectrum


def black_body(disp, area, temperature, **kwargs):
    """
    Return the spectral radiance of the surface of a black body.

    Parameters
    ----------
    disp : array_like
        The rest frame dispersion to return the flux at.
        If the attribute ``unit`` is not present, assumed in Hz.
    emitting_dust_mass : float
        The mass of dust in kg emitting at this temperature
    temperature : float
        The temperature of the black body (the only defining parameter).

    Returns
    -------
    Spectrum1D :
        The flux at each value in `disp`.
    """
    dust_mass_fraction = kwargs.pop('dust_mass_fraction', 1)

    #try:
    #    len_dmf = len(dust_mass_fraction)
    #except TypeError:  # Unsized object
    #    len_dmf = 1
    #try:
    #    len_temp = len(temperature)
    #except TypeError:
    #    len_temp = 1
    #if len_dmf is not len_temp:
    #    print("Inconsistant black_body component "
    #          "temperatures and dust mass fractions, exiting")
    #    sys.exit(1)

    freq = mathsu.to_unit(u.Hz, disp)
    try:
        freq = freq.value
    except AttributeError:  # NDData
        freq = freq.data

    # Change temperature, dust_mass_fraction to column vectors
    try:
        temperature = np.reshape(temperature, (len(temperature), 1))
        #dust_mass_fraction = np.reshape(dust_mass_fraction,
        #                                (len(dust_mass_fraction), 1))
    except TypeError:  # Scalars
        pass

    # Calculate BB luminosity density at each disp
    # A 2d array if multiple temperatures and dust mass fractions specified
    bodies = (dust_mass_fraction * 8 * np.pi * area *
              con.h.value / (con.c.value ** 2) * freq ** 3
              / (np.exp(con.h.value * freq
                        / (con.k_B.value * temperature)) - 1))
    #if len(bodies.shape) > 1:  # 2d array
    #    bodies = np.sum(bodies, axis=0)

    # Return a Spectrum1D
    try:  # disp is of type NDData
        spec = Spectrum1D(disp=disp,
                          flux=bodies * (u.W / u.m ** 2 / u.sr / u.Hz))
    except AttributeError:  #  disp has no unit
        print("Assuming disp in Hz in astro_utils.spec_utils.black_body")
        spec = Spectrum1D(disp=disp * u.Hz,
                          flux=bodies * (u.W / u.m ** 2 / u.sr / u.Hz))
    return spec


def blueshift(a, z, disp_type='frequency'):
    """
    Blueshift ``a`` back to its restframe dispersion.

    Parameters
    ----------
    a : array_like, astropy.nddata.NDData, or astropy.units.quantity.Quantity
        The value(s) to blueshift.
    z : float, optional
        The redshift of the emitting object (default=``0``).
    unit_type : str, optional
        If `a` is not a Quantity, does it describe ``'frequency'`` or
        ``'wavelength'`` (default=``'frequency'``).

    Returns
    -------
    ndarray or astropy.units.quantity.Quantity :
        The blueshifted value(s)
    """
    a_copy = copy.deepcopy(a)
    # Set modifyable part as ``data``
    if type(a_copy) is astropy.nddata.NDData:  # NDData
        if a.unit.is_equivalent(u.Hz):  # Frequency
            a_copy.data *= (1. + z)
        elif a.unit.is_equivalent(u.m):  # Wavelength
            a_copy.data /= (1. + z)
        else:
            print("``a`` cannot be blueshifted")
    else:
        try:  # Quantity
            if a.unit.is_equivalent(u.Hz):  # Frequency
                a_copy *= (1. + z)
            elif a.unit.is_equivalent(u.m):  # Wavelength
                a_copy /= (1. + z)
            else:
                print("``a`` cannot be blueshifted")
        except AttributeError:  # array_like
            if disp_type == 'wavelength':
                return np.asarray(a_copy, dtype=np.float64) / (1. + z)
            return np.asarray(a_copy, dtype=np.float64) * (1. + z)
    return a_copy


def blueshift_spectrum(self, z):
    """
    Blueshift ``self`` back to its restframe dispersion.

    Parameters
    ----------
    z : float
        The redshift of the emitting object.
    """
    try:  # Spectrum1D
        self.disp = blueshift(self.disp, z)
    except AttributeError:  # NDData or Quantity
        try:
            self.data = blueshift(self, z).data
        except AttributeError:
            self.value = blueshift(self, z).value

Spectrum1D.blueshift = blueshift_spectrum
astropy.nddata.NDData.blueshift = blueshift_spectrum
astropy.units.quantity.Quantity.blueshift = blueshift_spectrum


def bolometric_flux(spectrum):
    """
    Integrate flux density over the whole spectrum
    """
    return flux_density_to_flux(spectrum, lower_disp=None, upper_disp=None)

f_bol = bolometric_flux


def bolometric_luminosity(spectrum):
    """
    """
    return luminosity_density_to_luminosity(spectrum,
                                            lower_disp=None, upper_disp=None)

l_bol = bolometric_luminosity


def convert_dispersion(spectrum, convert_function):
    """
    Change the dispersion units via an input function

    Parameters
    ----------
    spectrum : Spectrum1D
        The spectrum to convert
    convert_function : function
        The relationship between a current dispersion value and the desired
        value
    """
    try:
        disp = convert_function(spectrum.disp)
        if type(disp) == type(spectrum.disp):
            spectrum.disp = disp
        else:
            spectrum.disp = disp
    except ValueError:  # ?
        for i in range(spectrum.size):
            spectrum.disp[i] = convert_function(spectrum.disp[i])
        if spectrum.disp[-1] < spectrum.disp[0]:
            spectrum.disp = spectrum.disp[::-1]
            spectrum.flux = spectrum.flux[::-1]

def convert_dispersion_spectrum(self, convert_function):
    spectrum = copy.deepcopy(self)
    convert_dispersion(spectrum, convert_function)
    return spectrum

Spectrum1D.convert_dispersion = convert_dispersion_spectrum


def disp_to_index(self, disp_value):
    """
    Returns the index of self.disp.data at value ``disp_value`` (slow).

    Only the first possibility is returned if several are possible.

    Parameters
    ----------
    disp_value : float
        The dispersion value to determine the index at.

    Returns
    -------
    float :
        The index value at ``disp_value``.
    """
    from scipy.interpolate import UnivariateSpline
    reversed_disp = False
    if self.disp.data[-1] < self.disp.data[0]:
        self.disp.data = self.disp.data[::-1]
        reversed_disp = True
    index_spline = UnivariateSpline(self.disp.data, np.arange(self.size))
    try:
        try:  # Quantity
            disp_value = disp_value.to(self.disp.unit).value
        except u.UnitsError:  # Need equivalency
            disp_value = disp_value.to(self.disp.unit,
                                       equivalencies=u.spectral()).value
    except AttributeError:  # float
        pass
    index_value = index_spline(disp_value)
    if reversed_disp:
        index_value = self.size - index_value - 1
        self.disp.data = self.disp.data[::-1]
    return index_value

Spectrum1D.disp_to_index = disp_to_index


def doppler_shift(velocity):
    """
    Multiply by this value (for positive velocity towards source)

    Parameters
    ----------
    velocity : astropy.units.quantity.Quantity or
        The velocity to shift by (in m/s unless specified by unit)

    Returns
    -------
    float :
        The special relativistic doppler shift
    """
    beta = (velocity / con.c).decompose().value
    return np.sqrt((1 - beta) / (1 + beta))


def emissivity(disp, area, spectral_emissivity_index, total_dust_mass):
    """
    Return the emissivity of a dusty source.

    See optical_depth for definition of constants.

    Parameters
    ----------
    disp : array_like (including astropy.nddata.NDData)
        The rest frame dispersion to measure the optical depth at.
        If the attribute ``units`` is not present, assumed in Hz
    area : float
        The projected area of the source in m**2,
    total_dust_mass : float
        The total mass of dust within the source in kg,
    spectral_emissivity_index : float
        Often denoted as 'beta', 1.8 is a standard value.

    Returns
    -------
    numpy.ndarray :
        The optical depth at each value in `disp`
    """
    return 1 - np.exp(-optical_depth(disp, area,
                                     spectral_emissivity_index,
                                     total_dust_mass,          ))


def fir_flux(spectrum, z, lower_disp=None, upper_disp=None):
    """
    Integrate under the far-infrared region of a spectrum
    (default rest-frame 40-500um)
    """
    if lower_disp is None:
        lower_disp = redshift(42.5 * u.um, z)
    if upper_disp is None:
        upper_disp = redshift(122.5 * u.um, z)
    return flux_density_to_flux(spectrum, lower_disp, upper_disp)

f_fir = fir_flux


def fir_luminosity(spectrum, lower_disp=(42.5*u.um), upper_disp=(122.5*u.um)):
    """
    Integrate under the far-infrared region of a spectrum (default 40-500 um)
    """
    return luminosity_density_to_luminosity(spectrum, lower_disp, upper_disp)

l_fir = fir_luminosity


def fir_radio_correlation(fir_lum, lum_dens_1_4GHz, norm_disp=(80 * u.um)):
    """
    Takes fir_luminosity (e.g. 40-120 um)
    mathsu.to_unit(u.Hz, 80 * u.um).value ~ 3.75e12
    """
    return np.log10((fir_lum / mathsu.to_unit(u.Hz, norm_disp).value)
                    / lum_dens_1_4GHz                               )


def flux_density_to_flux(spectrum, lower_disp, upper_disp):
    """
    Integrate under a spectrum between `lower_disp` and `upper_disp`.
    """

    # Slice spectrum to range to integrate over
    spectrum_copy = spectrum.slice_dispersion(lower_disp, upper_disp)

    # Change dispersion units to Hz
    try:
        freq = spectrum_copy.disp.unit.to(u.Hz, spectrum_copy.disp.value)
    except u.UnitsError:
        freq = spectrum_copy.disp.unit.to(u.Hz, spectrum_copy.disp.value,
                                           equivalencies=u.spectral()    )

    # Integrate from flux_density to flux and convert to luminosity
    from scipy.integrate import simps
    return abs(simps(spectrum_copy.flux.data, freq))


def flux_density_to_luminosity_density(spectrum, z, **kwargs):
    """
    Luminosity density (source frame) given the flux density (observed frame).

    Parameters
    ----------
    flux : array_like or Spectrum1D
        The flux (e.g. in W / m ** 2).
    magnification : float
        A flux magnification factor due to gravitational lensing.
    redshift : float
        The redshift of the source.
    Returns
    -------
    numpy.ndarray or Spectrum1D:
        The luminosity density (source frame).
    """
    magnification = kwargs.pop('magnification', 1)
    blueshifted_spectrum = copy.deepcopy(spectrum)
    blueshifted_spectrum.blueshift(z)
    luminosity = flux_to_luminosity(blueshifted_spectrum, z,
                                    magnification=magnification)
    try:  # array_like
        return luminosity / (1 + z)
    except TypeError:  # Spectrum1D
        luminosity.flux /= (1 + z)
        try:
            luminosity.uncertainty /= (1 + z)
        except AttributeError:
            pass
        return luminosity


def flux_to_flux(flux, z, dispersion):
    """
    Convert flux in Jy km s^-1 to W m^-2 or vice versa

    If units are not given (astropy.units.Quantity), assume in Jy km s^-1

    Parameters
    ----------
    flux :

    z :

    dispersion : astropy.units.Quantity or float
        The observed dispersion, if a float, assume in GHz.

    Returns
    -------
    astropy.units.quantity.Quantity :
        The converted flux
    """
    # First get dispersion into um
    dispersion = mathsu.to_unit(u.um, dispersion)

    try:
        if flux.unit.is_equivalent(u.W / u.m ** 2):
            return (flux * 1e17 *  dispersion).value * u.Jy * u.km / u.s
        else:
            raise AttributeError
    except AttributeError:  # Jy km s ** -1
        return (flux * 1e-17 / dispersion).value * u.W / u.m ** 2


def flux_to_luminosity(fluxes, z, **kwargs):
    """
    Return the luminosity (source frame) given the flux (observed frame).

    Parameters
    ----------
    flux : array_like or Spectrum1D
        The flux (e.g. in W / m ** 2) or a spectrum.
    magnification : float
        A flux magnification factor due to gravitational lensing.
    redshift : float
        The redshift of the source.
    Returns
    -------
    numpy.ndarray or Spectrum1D:
        The luminosity (source frame).
    """
    magnification = kwargs.pop('magnification', 1)
    luminosity_distance = cosmology.luminosity_distance(z)
    luminosity_distance = luminosity_distance.to(u.m)
    if type(fluxes) is np.ndarray:
        return (fluxes * 4 * np.pi * luminosity_distance.value ** 2
                / magnification                                          )
    try:  # quantity
        return (fluxes * 4 * np.pi * luminosity_distance ** 2
                * (1 / magnification))
    except TypeError:  # Spectrum1D
        #flux = (fluxes.flux.data * 4 * np.pi
        #        * luminosity_distance.value ** 2 / magnification)
        #return Spectrum1D(disp=fluxes.disp,
        #                  flux=NDData(flux, unit=fluxes.flux.unit))
        luminosity = copy.deepcopy(fluxes)
        luminosity.flux = (fluxes.flux * 4 * np.pi
                * luminosity_distance ** 2 / magnification)
        try:
            luminosity.uncertainty = (fluxes.uncertainty * 4 * np.pi
                    * luminosity_distance ** 2 / magnification)
        except AttributeError:
            pass
        return luminosity


def get_hipe_scans_from_file(path, off_axis=False, off_axis_to_ignore=None):
    """
    Generate two Spectrum1D from a fits file produced by HIPE (SLW, SSW)

    Spectrum1D flux.data are 2D arrays (one row for each scan)
    Also with optional parameters, get the mean


    Parameters
    ----------
    path : string
        The path to the file
    off_axis : boolean
        Return off-axis scans as well
    off_axis_to_ignore : list
        Off axis detectors to ignore when calculating mean off-axis spectrum

    Returns
    -------
    Spectrum1D, Spectrum1D :
        SLW, SSW - the generated spectra
    """
    #from astropy.io import fits
    hdulist = fits.open(str(path))
    hdulist.verify('silentfix')

    # Can't just use header['ds_1'] - header['ds_0'] - 1 in case of filtering
    # Alternatively can compare against central and off-axis detectors
    #detectors_to_keep = ['slwc3', 'sswd4', "sswc2", "sswb3", "sswc5", "sswf3",
    #                    "sswe2", "sswe5", 'slwd3', "slwc4", "slwb3", "slwb2",
    #                    "slwc2", "slwd2"]
    number_detectors = 0
    for index, hdu in enumerate(hdulist[2:]):
        if 'DSETS___' in hdu.header:
            number_detectors = index
            break

    # probably 200 scans (100 * forward+reverse) for each detector
    try:
        number_scans = hdulist[0].header['numscans']
        detectors = [(index + 1, hdu.header['chnlname'].lower().strip())
                for index, hdu in enumerate(hdulist[2:number_detectors + 2])]
    except KeyError:
        number_scans = hdulist[0].header['meta_9'] * 2
        detectors = [(index + 1, hdu.header['meta_0'].lower())
                for index, hdu in enumerate(hdulist[2:number_detectors + 2])]

    #print("number_scans =", number_scans, "detectors", detectors)

    # DO NOT NEED THIS ANY MORE
    # correct for doppler shift due to radial velocity of telecope
    # positive velocity towards source means we underestimate redshift
    # Doppler factor is <1 for positive radial velocity
    # - need to redshift it more to get lsr - decrease frequency
    #radial_velocity = hdulist[0].header['vframe'] * u.km / u.s
    #doppler_factor = doppler_shift(radial_velocity)

    # Fill disp.data but leave flux.data empty (stored in many hdus)
    slw_index = [detector[0] for detector in detectors if detector[1] == 'slwc3'][0]

    slw = Spectrum1D(
            disp=hdulist[1 + slw_index].data['wave'] * u.GHz, # * doppler_factor,
            flux=np.empty((number_scans, len(hdulist[1 + slw_index].data))) * u.Jy,
            )
    ssw_index = [detector[0] for detector in detectors if detector[1] == 'sswd4'][0]
    ssw = Spectrum1D(
            disp=hdulist[1 + ssw_index].data['wave'] * u.GHz, # * doppler_factor,
            flux=np.empty((number_scans, len(hdulist[1 + ssw_index].data))) * u.Jy,
            )

    # Generate lists of numbers for allowed slw, ssw off-axis scans
    allowed_off_axis_slw = []
    allowed_off_axis_ssw = []
    if off_axis_to_ignore:
        off_axis = True
    if off_axis == True:
        if not off_axis_to_ignore:
            off_axis_to_ignore = []
        #for detector in detectors:
        #    print("detector =", detector)
        allowed_off_axis_slw = [
                detector[0] for detector in detectors
                if detector[1][:3] == 'slw'
                and detector[1] not in ([off_axis_to_ignore] + ['slwc3'])
                ]
        allowed_off_axis_ssw = [
                detector[0] for detector in detectors
                if detector[1][:3] == 'ssw'
                and detector[1] not in ([off_axis_to_ignore] + ['sswd4'])
                ]

        slw_off_axis = Spectrum1D(
                disp=hdulist[1 + slw_index].data['wave'] * u.GHz, # * doppler_factor,
                flux=np.empty((number_scans, len(hdulist[1 + slw_index].data))) * u.Jy,
                )
        ssw_off_axis = Spectrum1D(
                disp=hdulist[1 + ssw_index].data['wave'] * u.GHz, # * doppler_factor,
                flux=np.empty((number_scans, len(hdulist[1 + ssw_index].data))) * u.Jy,
                )

    # Each background scan consists of up to 6 off-axis detectors to average
    slw_off_axis_scans = np.empty((len(allowed_off_axis_slw),
                                   len(hdulist[1 + slw_index].data)      ))
    ssw_off_axis_scans = np.empty((len(allowed_off_axis_ssw),
                                   len(hdulist[1 + ssw_index].data)      ))

    # Fill flux.data arrays
    for index, hdu_number in enumerate(
            range(1,
                  1 + (number_detectors + 1) * number_scans,
                  number_detectors + 1)):

        # Central detectors
        slw.flux.value[index] = hdulist[hdu_number + slw_index].data['flux']
        del(hdulist[hdu_number + slw_index].data)
        ssw.flux.value[index] = hdulist[hdu_number + ssw_index].data['flux']
        del(hdulist[hdu_number + ssw_index].data)

        # Off-axis detectors
        if off_axis:

            # Get the (up to 6) off-axis scans
            for index_allowed_slw, allowed_off_axis_number \
                    in enumerate(allowed_off_axis_slw):
                slw_off_axis_scans[index_allowed_slw] = (
                        hdulist[hdu_number +
                                allowed_off_axis_number].data['flux'])
                del(hdulist[hdu_number + allowed_off_axis_number].data)
            # Average these to make a `background' scan
            slw_off_axis.flux.value[index] = np.mean(slw_off_axis_scans, axis=0)

            # Same for ssw
            for index_allowed_ssw, allowed_off_axis_number \
                    in enumerate(allowed_off_axis_ssw):
                ssw_off_axis_scans[index_allowed_ssw] = (
                        hdulist[hdu_number +
                                allowed_off_axis_number].data['flux'])
                del(hdulist[hdu_number + allowed_off_axis_number].data)
            ssw_off_axis.flux.value[index] = np.mean(ssw_off_axis_scans, axis=0)

    if off_axis:
        return slw, ssw, slw_off_axis, ssw_off_axis
    else:
        return slw, ssw


def get_hipe_spectrum_from_file(path):
    """
    Generate a Spectrum1D from a fits file produced by HIPE (combines arrays)

    Parameters
    ----------
    path : string
        The path to the file

    Returns
    -------
    Spectrum1D
        The generated spectrum
    """
    slw, ssw = get_raw_hipe_spectrum_from_file(path)

    # Add the two baseline-subtracted spectra
    return add_spectra([baseline_subtract(slw), baseline_subtract(ssw)],
                       weighting='inv_var', window='hanning', size=500  )


def get_raw_hipe_spectrum_from_file(path):
    """
    Generate a Spectrum1D for each array from a fits file produced by HIPE

    Parameters
    ----------
    path : string
        The path to the file

    Returns
    -------
    Spectrum1D, Spectrum1D
        The generated spectra (SLW, SSW)
     """
    # Get spectra and construct Spectrum1D objects to hold them
    tab_ssw = fits.open(path)
    tab_ssw.verify('silentfix')
    #radial_velocity = tab_ssw[0].header['vframe'] * u.km / u.s
    tab_slw = [hdu.data for hdu in tab_ssw if
               (hdu.header.get('chnlname', default='') == 'SLWC3'
                or hdu.header.get('meta_0', default='') == 'SLWC3')
               ][0]
    tab_ssw = [hdu.data for hdu in tab_ssw if
               (hdu.header.get('chnlname', default='') == 'SSWD4'
                or hdu.header.get('meta_0', default='') == 'SSWD4')
               ][0]
    slw = Spectrum1D(disp=tab_slw['wave'] * u.GHz,
                     flux=tab_slw['flux'] * u.Jy  )
    ssw = Spectrum1D(disp=tab_ssw['wave'] * u.GHz,
                     flux=tab_ssw['flux'] * u.Jy  )

    # No longer need to correct for radial velocity
    # correct for doppler shift due to radial velocity of telecope
    # positive = motion towards source => need to redshift
    #doppler_factor = doppler_shift(radial_velocity)
    #slw.disp.data *= doppler_factor
    #ssw.disp.data *= doppler_factor

    return slw, ssw


def grey_body(disp, area, temperature, total_dust_mass, **kwargs):
    """
    Return the spectral radiance of the surface of a grey body.

    Parameters
    ----------
    disp : array_like
        The rest frame dispersion to return the flux at.
        If the attribute ``unit`` is not present, assumed in Hz.
    area : float
        The projected area of the source in m**2,
        (default=``7.478e38``, circle of 1e3 pc diameter).
    temperature : float
        The temperature of the emitting dust.
    total_dust_mass : float
        The mass of dust absorbing emission.
    spectral_emissivity_index : float, optional
        Often denoted as 'beta', (default=``1.8``).

    Returns
    -------
    Spectrum1D :
        Contains the flux at each value in `disp`.
    """
    dust_mass_fraction = kwargs.pop('dust_mass_fraction', 1)
    spectral_emissivity_index=kwargs.pop('spectral_emissivity_index', 1.8)

    spec = black_body(disp, area, temperature,
                      dust_mass_fraction=dust_mass_fraction)

    spec.flux *= emissivity(disp, area, spectral_emissivity_index, total_dust_mass)

    return spec


def index_to_disp(self, index):
    """
    Returns the value of self.disp.data at index ``index`` (slow).

    Parameters
    ----------
    index : float
        The index to determine the dispersion value at.

    Returns
    -------
    astropy.units.quantity.Quantity :
        The dispersion value at ``index``.
    """
    from scipy.interpolate import UnivariateSpline
    value = UnivariateSpline(np.arange(self.size),
                             self.disp.value,      )(index)
    return value * self.disp.unit

Spectrum1D.index_to_disp = index_to_disp


def interpolate_flux(self, disp):
    """

    """
    try:  # astropy.units.quantity.Quantity
        disp = disp.to(self.disp.unit).value
    except u.UnitsError:
        disp = disp.to(self.disp.unit, equivalencies=u.spectral()).value
    except AttributeError:  # disp has no ``unit`` attribute
        pass

    # Interpolates best in log space, convert back afterwards
    from scipy.interpolate import UnivariateSpline
    flux = UnivariateSpline(np.log10(self.disp.value),
                            np.log10(self.flux.value), s=0)(np.log10(disp))
    return 10 ** flux

Spectrum1D.interpolate_flux = interpolate_flux


def interpolate_spectra_to_same_dispersion(spectrum_list):
    """ """
    # Each spectrum is fitted with an interpolating spline and sampled at the
    # output dispersion values

    # Create empty dispersion and flux arrays, assuming all dispersion arrays
    # have the same units
    # Total spectrum bin width = mean bin width across spectra
    # Could probably generalise this to dispersion dependent widths but meh
    interpolated_spectrum_list = copy.deepcopy(spectrum_list)

    bin_width = np.abs(np.mean(
        np.concatenate([(spec.disp.value[1:] - spec.disp.value[:-1] )
                                        for spec in spectrum_list])
        ))
    disp_min = min([np.min(spec.disp.value) for spec in spectrum_list])
    disp_max = max([np.max(spec.disp.value) for spec in spectrum_list])
    disp = np.array([]) * spectrum_list[0].disp.unit

    # If Dispersion increases along array
    if spectrum_list[0].disp.value[0] < spectrum_list[0].disp.value[-1]:
        disp = np.arange(disp_min, disp_max, bin_width) * disp.unit
    else:  # Dispersion decreases along array
        disp = np.arange(disp_max, disp_min, -bin_width) * disp.unit

    for index, spectrum in enumerate(interpolated_spectrum_list):

        # Indices of disp covered by spectrum_list[spec]
        disp_range = np.where(
            (disp.value >= (np.min(spectrum.disp.value) - bin_width / 10000)) &
            (disp.value <= (np.max(spectrum.disp.value) + bin_width / 10000))
            )[0]

        # Create empty flux, uncertainty arrays for final spectrum
        flux = np.zeros(spectrum.flux.shape[:-1] + disp.value.shape)
        if len(flux.shape) > 1:
            for scan_index, scan_array in enumerate(flux):
                # Fill the relevant indices of interpolated_spec with interpolated flux
                flux[scan_index][disp_range] = (
                      UnivariateSpline(spectrum.disp.value,
                          spectrum.flux.value[scan_index], s=0)
                      )(disp.value[disp_range])
        else:
            # Fill the relevant indices of interpolated_spec with interpolated flux
            flux[disp_range] = (
                  UnivariateSpline(spectrum.disp.value, spectrum.flux.value, s=0)
                  )(disp.value[disp_range])

        # Similarly for uncertainty (but only using 1d uncertainty)
        uncertainty = np.zeros(disp.value.size)
        uncertainty[disp_range] = (
              UnivariateSpline(spectrum.disp.value,
                               spectrum.uncertainty.value, s=0)
              )(disp.value[disp_range])

        spectrum.disp = copy.deepcopy(disp)
        spectrum.flux = flux * spectrum.flux.unit
        spectrum.uncertainty = uncertainty * spectrum.flux.unit


    return interpolated_spectrum_list


def ir_flux(spectrum, z, lower_disp=None, upper_disp=None):
    """
    Integrate under the far-infrared region of a spectrum
    (default rest-frame 8-1000um)
    """
    if lower_disp is None:
        lower_disp = redshift(8 * u.um, z)
    if upper_disp is None:
        upper_disp = redshift(1000 * u.um, z)
    return flux_density_to_flux(spectrum, lower_disp, upper_disp)

f_ir = ir_flux


def ir_luminosity(spectrum, lower_disp=(8 * u.um), upper_disp=(1000 * u.um)):
    """
    Integrate under the far-infrared region of a spectrum (default 8-1000 um)
    """
    return luminosity_density_to_luminosity(spectrum, lower_disp, upper_disp)

l_ir = ir_luminosity


def line_flux(self, disp, slice_width=100, velocity_zero_point=None, **kwargs):
    """
    Return the total flux (in W m^-2) of a Gaussian/sinc fitted at ``disp``.
    """
    components = kwargs.pop('components', 1)  # number of components to fit
    function = kwargs.pop('function', 'gaussian')
    restrictions = kwargs.pop('restrictions', None)
    sigma = kwargs.pop('sigma', None)
    # Find the index corresponding to `disp`
    index = self.disp_to_index(disp)

    # Cut a section of the spectrum around the predicted line position
    try:
        cut_spectrum = self.slice_index(index - slice_width,
                                        index + slice_width )
    except TypeError:  # slice_width is None
        cut_spectrum = self

    # Since `index` may be within `slice_width` of one end of `self`, `disp`
    # cannot be assumed to be at index `slice_width` of `cut_spectrum`.
    # Find the new index corresponding to ``disp``
    cut_index = cut_spectrum.disp_to_index(disp)

    # mean and sigma are required to be floats in self.disp.unit
    # Initial guess at mean
    try:
        mean = disp.to(self.disp.unit).value
    except u.UnitsError:
        mean = disp.to(self.disp.unit, equivalencies=u.spectral()).value
    except AttributeError:
        mean = disp
    # Initial guess at sigma = 2 indices
    if not sigma:
        sigma = abs(cut_spectrum.index_to_disp(cut_index + 2).value - mean)
    # Initial guess at amplitude
    amplitude = self.flux.value[int(index)]

    # Create list of lists of component parameters
    means = np.arange(mean - (sigma * (components - 1)),
                      mean + (sigma * (components - 1)) + 1, 2 * sigma)
    initial_parameters = []
    for component_mean in means:
        initial_parameters.append([component_mean, sigma, amplitude])
    # base
    initial_parameters.append(0)

    # If dispersion in km / s, require velocity_zero_point
    if self.disp.unit == u.km / u.s:
        if velocity_zero_point is None:
            sys.exit("velocity_zero_point required for dispersion in km / s")
        else:
            velocity_zero_point = mathsu.to_unit(u.Hz, velocity_zero_point)

    # Unit area calculation
    def unit_area(mean):
        if self.disp.unit is u.Hz:
            unit_area = 1e-26
        elif self.disp.unit is u.GHz:
            unit_area = 1e-17
        elif self.disp.unit is u.um:
            unit_area = (con.c * 1e-20 / (mean ** 2)).value
        elif self.disp.unit == u.km / u.s:
            unit_area = (1e-23 * velocity_zero_point / con.c).value
        return unit_area

    # Fit Gaussian to line
    from scipy.optimize import curve_fit
    def fitted_flux(flux_array):
        if function == 'gaussian':
            popt = mathsu.fit_gaussian(cut_spectrum.disp.value, flux_array,
                                       p0=(initial_parameters),
                                       restrictions=restrictions,
                                       )

            # popt is a list of mean, sigma, amplitude groups,
            # followed by base
            total = 0
            for group in popt[:-1]:
                total += (unit_area(group[0])
                          * mathsu.area_gaussian(group[2], group[1]))

        if function == 'sinc':
            popt = mathsu.fit_sinc(cut_spectrum.disp.value, flux_array,
                                   p0=(initial_parameters),
                                   restrictions=restrictions,
                                   )

            # popt is a list of mean, sigma, amplitude groups,
            # followed by base

            #print("popt =", popt)
            #import matplotlib.pyplot as plt
            #plt.plot(cut_spectrum.disp.data, flux_array)
            #plt.plot(cut_spectrum.disp.data,
            #         mathsu.sinc(cut_spectrum.disp.data, *popt))
            #plt.show()

            total = 0
            for group in popt[:-1]:
                total += (unit_area(group[0])
                          * mathsu.area_sinc(group[2], group[1]))

        return total

    flux = fitted_flux(cut_spectrum.flux.value)

    # Find variance at disp
    variance = self.uncertainty.value[int(index)] ** 2
    # Assume all points in cut_spectrum have the same variance
    covariance = np.diag([variance] * len(cut_spectrum))
    flux_var = mathsu.covariance(
            fitted_flux, [cut_spectrum.flux.value], covariance)

    return (flux, np.sqrt(flux_var))

Spectrum1D.line_flux = line_flux


def lines_in_range(low_disp=0, high_disp=1, disp_type='wavelength'):
    """
    Return a list of the lines over some range, taken from 'lines.txt'

    Parameters
    ----------
    low_disp : float
        The minimum wavelength of the range (in micrometres)
    high_disp : float
        The maximum wavelength of the range (in micrometres)

    Returns
    -------
    list
        A list of the spectral lines in the range with the following
        properties:
            list[i][0]: Wavelength of line[i]
            list[i][1]: Name of the species producing the line
            list[i][2]: State of the transition
    """
    # Add all lines to the list ``lines``
    lines_path = (os.path.dirname(os.path.realpath(__file__)) +
                  '/spectral_lines.txt')
    lines_file = open(lines_path, 'r')
    wavelength_lines = lines_file.readlines()
    lines = []
    for i in wavelength_lines:
        lines.append(i.split())

    # Make sure low_disp, high_disp are minimum, maximum wavelength
    if disp_type in ('frequency', 'freq'):
        low_disp = ghz_to_um(low_disp)
        high_disp = ghz_to_um(high_disp)
    low_disp, high_disp = min(low_disp, high_disp), max(low_disp, high_disp)

    # Filter the list selecting only those lines within range
    spectral_lines = [lines[i] for i in range(len(lines))
                      if float(lines[i][0]) > low_disp
                         and float(lines[i][0]) < high_disp]
    return spectral_lines


def luminosity_density_to_flux_density(lum_density, z, **kwargs):
    """
    Return the flux density (observed frame) given the luminosity density.

    Parameters
    ----------
    lum_density : array_like
        The luminosity per unit rest frame frequency.
    magnification : float
        A flux magnification factor due to gravitational lensing.
    redshift : float
        The redshift of the source.
    Returns
    -------
    numpy.ndarray :
        The observed frame flux per unit observed frame frequency.
    """
    magnification = kwargs.pop('magnification', 1)
    redshifted_spectrum = copy.deepcopy(lum_density)
    redshifted_spectrum.redshift(z)
    redshifted_spectrum = luminosity_to_flux(redshifted_spectrum, z,
                                             magnification=magnification)
    redshifted_spectrum.flux *= (1 + z)
    try:
        redshifted_spectrum.uncertainty *= (1 + z)
    except AttributeError:
        pass
    return redshifted_spectrum


def luminosity_density_to_luminosity(spectrum, lower_disp, upper_disp):
    """
    Integrate luminosity density over a dispersion range to a luminosity.

    Parameters
    ----------
    lower_disp :

    upper_disp

    Returns
    -------
    numpy.ndarray :
        The luminosity emitted across the given dispersion range
    """
    return flux_density_to_flux(spectrum, lower_disp, upper_disp)


def luminosity_to_flux(luminosity, z, **kwargs):
    """
    Return the flux (observed frame) given the luminosity.

    Parameters
    ----------
    luminosity : array_like
        The luminosity (source frame).
    magnification : float
        A flux magnification factor due to gravitational lensing.
    redshift : float
        The redshift of the source.
    Returns
    -------
    numpy.ndarray :
        The observed frame flux .
    """
    magnification = kwargs.pop('magnification', 1)
    from astropy.cosmology import WMAP7
    luminosity_distance = WMAP7.luminosity_distance(z).to(u.m)

    try:
        return (luminosity * magnification
                / (4 * np.pi * luminosity_distance ** 2))
    except TypeError:
        fluxes = copy.deepcopy(luminosity)
        fluxes.flux = luminosity_to_flux(
                luminosity.flux, z, magnification=magnification)
        try:
            fluxes.uncertainty = luminosity_to_flux(
                    luminosity.uncertainty, z, magnification=magnification)
        #fluxes.flux = (luminosity.flux * magnification
        #        / (4 * np.pi * luminosity_distance ** 2))
        #try:
        #    fluxes.uncertainty = (luminosity.uncertainty * magnification
        #            / (4 * np.pi * luminosity_distance ** 2))
        except AttributeError:
            pass
        return fluxes


def observed_grey_body(disp, area, temperature, total_dust_mass, z, **kwargs):
    """
    disp :
        Observed frame dispersion

    Returns
    -------

        flux density
    """
    dust_mass_fraction = kwargs.pop('dust_mass_fraction', 1)
    magnification = kwargs.pop('magnification', 1)
    spectral_emissivity_index = kwargs.pop('spectral_emissivity_index', 1.8)

    rest_frame_disp = blueshift(disp, z)
    spec = grey_body(rest_frame_disp,
                     area=area,
                     dust_mass_fraction=dust_mass_fraction,
                     spectral_emissivity_index=spectral_emissivity_index,
                     temperature=temperature,
                     total_dust_mass=total_dust_mass,
                     )

    return luminosity_density_to_flux_density(spec, z,
                                              magnification=magnification)


def observed_power_law_temp_dust_model(
        disp, area, minimum_temperature, total_dust_mass, z, **kwargs):
    """
    disp must be in Hz
    """
    gamma = kwargs.get('dust_mass_temperature_index', 7.2)  # Shorter to type
    magnification = kwargs.pop('magnification', 1)
    spectral_emissivity_index = kwargs.pop('spectral_emissivity_index', 1.8)
    #print("obs", area, minimum_temperature, total_dust_mass, z, gamma, magnification, spectral_emissivity_index)

    # Since this has a lot of custom code, need to ensure disp in Hz
    rest_frame_freq = blueshift(mathsu.to_unit(u.Hz, disp), z)

    if type(rest_frame_freq) is astropy.nddata.NDData:
        rest_frame_freq = rest_frame_freq.data
    else:  # Quantity
        try:
            rest_frame_freq = rest_frame_freq.value
        except AttributeError:  # ndarray
            pass

    # The form of the multi- grey-body function could be found by summing
    # several discrete grey bodies, but it can also be found analytically
    # using the incomplete Riemann zeta function (see above for definition
    # of ``integral``).

    # Calculate power-law grey body luminosity density
    lum_dens = (8 * np.pi * con.h.value / (con.c.value ** 2)
                * (gamma - 1)
                * rest_frame_freq ** 4
                * (con.h.value / (con.k_B.value * minimum_temperature))
                * (con.k_B.value * minimum_temperature
                   / (con.h.value * rest_frame_freq)  ) ** gamma
                * area
                * emissivity(rest_frame_freq, area,
                             spectral_emissivity_index, total_dust_mass)
                #* emissivity(rest_frame_freq, are, 1.8, total_dust_mass)
                * mathsu.incomplete_riemann_zeta(
                    rest_frame_freq, gamma, minimum_temperature)
                )

    #spec = Spectrum1D(disp=NDData(rest_frame_freq, unit=u.Hz),
    #                  flux=NDData(lum_dens, unit=(u.W / u.Hz)))
    spec = Spectrum1D(disp=rest_frame_freq * u.Hz,
                      flux=lum_dens * u.W / u.Hz)

    # Convert to flux density
    spec = luminosity_density_to_flux_density(spec, z,
                                              magnification=magnification)
    return spec


def observed_synchrotron(disp, scalar, spectral_index, z, **kwargs):
    """
    """
    magnification = kwargs.pop('magnification', 1)

    rest_frame_disp = blueshift(disp, z)
    spec = synchrotron(rest_frame_disp, scalar=scalar,
                       spectral_index=spectral_index  )

    return luminosity_density_to_flux_density(spec, z, magnification=magnification)


def optical_depth(disp, area, spectral_emissivity_index, total_dust_mass):
    """
    Return the optical depth to radiation leaving a dusty source.

    Uses a characteristic photon cross-section to mass ratio of particles of
    kappa_850um = 0.15 m**2 kg**-1.

    Parameters
    ----------
    disp : array_like (including astropy.nddata.NDData)
        The rest frame dispersion to measure the optical depth at.
        If the attribute ``unit`` is not present, assumed in Hz
    area : float
        The projected area of the source in m**2,
    spectral_emissivity_index : float
        Often denoted as 'beta', 1.8 is a standard value.
    total_dust_mass : float
        The total mass of dust within the source in kg,

    Returns
    -------
    numpy.ndarray :
        The optical depth at each value in `disp`
    """
    freq = mathsu.to_unit(u.Hz, disp)
    if type(freq) is astropy.nddata.NDData:
        freq = freq.data
    else:  # Quantity
        freq = freq.value
    #return (0.15 * total_dust_mass / area
    #        * (freq * 850e-6 / con.c.value) ** spectral_emissivity_index)
    return (0.077 * total_dust_mass / area
            * (freq * 850e-6 / con.c.value) ** spectral_emissivity_index)


def redshift(a, z=0, disp_type='frequency'):
    """
    Redshift ``a`` back to its observed dispersion.

    Parameters
    ----------
    a : array_like, astropy.nddata.NDData, or astropy.units.quantity.Quantity
        The value(s) to redshift.
    z : float, optional
        The redshift of the emitting object (default=``0``).
    unit_type : str, optional
        If `a` is not a Quantity, does it describe ``'frequency'`` or
        ``'wavelength'`` (default=``'frequency'``).

    Returns
    -------
    ndarray or astropy.units.quantity.Quantity :
        The redshifted value(s)
    """
    a_copy = copy.deepcopy(a)
    # Set modifyable part as ``data``
    try:  # Quantity
        if a.unit.is_equivalent(u.Hz):  # Frequency
            a_copy /= (1. + z)
        elif a.unit.is_equivalent(u.m):  # Wavelength
            a_copy *= (1. + z)
        else:
            print("``a`` cannot be redshifted")
    except TypeError:  # NDData
        if a.unit.is_equivalent(u.Hz):  # Frequency
            a_copy.data /= (1. + z)
        elif a.unit.is_equivalent(u.m):  # Wavelength
            a_copy.data *= (1. + z)
        else:
            print("``a`` cannot be redshifted")

    except AttributeError:  # array_like
        if disp_type == 'wavelength':
            return np.array(a_copy, dtype=np.float64) * (1. + z)
        return np.array(a_copy, dtype=np.float64) / (1. + z)
    return a_copy


def redshift_spectrum(self, z):
    """
    Redshift ``self`` back to its observed dispersion.

    Parameters
    ----------
    z : float
        The redshift of the emitting object.

    Returns
    -------
    ndarray :
        ``a`` redshifted by ``redshift``
    """
    try:  # Spectrum1D
        self.disp = redshift(self.disp, z)
    except AttributeError:  # NDData or Quantity
        self = redshift(self, z)
        #try:
        #    self.data = redshift(self, z).data
        #except AttributeError:
        #    self.value = redshift(self, z).value

Spectrum1D.redshift = redshift_spectrum
astropy.nddata.NDData.redshift = redshift_spectrum
astropy.units.quantity.Quantity.redshift = redshift_spectrum


def sort_disp(self, reverse=False):
    """
    Sort all spectrum points by dispersion value.

    Parameters
    ----------
    reverse : bool, optional
        Sort in a descending order rather than ascending, (default=``False``).
    """
    # Each flux and uncertainty element can be sorted along with disp by sorting
    # a numpy recarray
    try:
        spec = np.core.records.fromarrays(
                [self.disp.value, self.flux.value, self.uncertainty.value],
                names=['disp', 'flux', 'uncertainty']
                )
    except AttributeError:  # No uncertainty
        spec = np.core.records.fromarrays(
                [self.disp.value, self.flux.value],
                names=['disp', 'flux']
                )
    spec = np.sort(spec, order=['disp'])

    # Is decreasing dispersion desired?
    if reverse is True:
        spec = spec[::-1]

    # Write back to self
    self.disp = spec['disp'] * self.disp.unit
    self.flux = spec['flux'] * self.flux.unit
    try:
        #self.uncertainty.data = spec['uncertainty']
        self.uncertainty = spec['uncertainty'] * self.uncertainty.unit
    except ValueError:  # No uncertainty
        pass

Spectrum1D.sort_disp = sort_disp


def synchrotron(disp, scalar, spectral_index):
    """
    Return the luminosity density of synchrotron emission at dispersion `disp`.
    flux = scalar * disp ** -spectral_index

    Parameters
    ----------
    scalar : float
        A typical value is ``1e30``.
    spectral_index : float
        A typical value is ``-0.75``.

    Returns
    -------
    numpy.ndarray :
        The flux at each value in `disp`.
    """
    freq = mathsu.to_unit(u.Hz, disp)
    try:  # NDData
        spec = Spectrum1D(disp=freq,
                          flux=scalar * freq.value ** spectral_index * u.W / u.Hz)
                          #flux=NDData(scalar * freq.data ** -spectral_index,
                          #            unit=(u.W / u.Hz)                    ))
    except AttributeError:  # Quantity
        #spec = Spectrum1D(disp=NDData(freq.value, unit=u.Hz),
        #                  flux=NDData(scalar * freq.value ** -spectral_index,
        #                              unit=(u.W / u.Hz)                     ))
        spec = Spectrum1D(disp=freq.value * u.Hz,
                          flux=scalar * freq.value ** spectral_index * u.W / u.Hz)
    return spec
