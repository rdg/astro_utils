"""
This module contains useful photometry functions such as calculating the flux
of a source in an image after subtracting the sky within a surrounding annulus.
"""
from __future__ import division, print_function

import numpy as np
import photutils
import astropy.units as u

import astro_utils.maths_utils as mathsu

class InnerError(Exception):
    pass


def angle_to_pixel_diff(angle, wcs):
    """
    Convert an angle to a number of pixels given a WCS.

    Parameters
    ----------
    angle : astropy.coordinates.angles.Angle
        An angle representing a distance in an image.
    wcs : astropy.wcs.WCS
        A world coordinate system.

    Returns
    -------
    astropy.units.quantity.Quantity :
        A number of pixels equivalent to the input angle.
    """
    # pixels per degree = 1 / wcs.wcs.cdelt[1]
    try:
        return angle.degree / abs(wcs.wcs.cdelt[1]) * u.pix
    except AttributeError:  # Not an Angle
        try:
            return angle.to(u.deg) / abs(wcs.wcs.cdelt[1]) / u.deg * u.pix
        except AttributeError:
            return angle * u.pix


def aperture_flux(image, **kwargs):
    """
    Just the flux inside a circle, requires some background subtraction
    """
    source_points = kwargs.get('source_points', 'centre')  # can be a list
    wcs = kwargs.get('wcs')

    # Fix the non-filled areas of the image
    image[np.isnan(image)] = 0

    # Generate initial source position guess (maximum flux or centre of image)
    # image.shape[0] = y_values, image.shape[1] = x_values

    import collections
    if (isinstance(source_points, collections.Iterable)
            and type(source_points) != str             ):
        return aperture_flux_multiple_source(image, **kwargs)

    # Else is a single coordinate object or separate ra, dec
    position = source_points
    y_c, x_c = pixel_coordinates_at_position(image, position, wcs=wcs)
    radius = angle_to_pixel_diff(kwargs.get('radius'), wcs=wcs)

    # If the aperture is to be centred on the source
    if kwargs.get('centre', False):
        # Fit a gaussian to image (assuming unresolved point source)
        from agpy.gaussfitter import gaussfit
        gauss_params = gaussfit(image, params=[0, image[y_c, x_c],
                                               x_c, y_c, 2, 2, 0] )

        # Use fitted parameters for positions for flux calculations
        x_c, y_c = gauss_params[2:4]      # x, y from gaussfit

        # radius needs to be a float number of pixels
        if radius is None:
            sigma = max(gauss_params[4:6])  # x, y sigmas from gaussfit
            radius = kwargs.get('scale', 3) * sigma

    # Using these parameters, calculate the flux
    aperture = photutils.CircularAperture((x_c, y_c), radius.value)
    total_flux = photutils.aperture_photometry(image, aperture)
    total_flux = total_flux[0]['aperture_sum']
    total_area = aperture.area()

    if kwargs.get('area', False):  # return area as well as flux
        return total_flux, total_area
    return total_flux


def aperture_flux_multiple_source(image, **kwargs):
    """
    Return flux of both individual sources + total
    """
    source_points = kwargs.get('source_points')
    radius = kwargs.get('radius')
    wcs = kwargs.get('wcs')
    results = {}

    # Assuming a list of ICRSCoordinates
    for index, coord in enumerate(source_points):
        flux, area = aperture_flux(image, source_points=coord,
                                   radius=radius[index],
                                   wcs=wcs,
                                   area=True,
                                   )
        results['F{0}'.format(index)] = flux
        results['A{0}'.format(index)] = area

    # Now need to estimate any overlapping regions
    import itertools, math
    y0, x0 = pixel_coordinates_at_position(image, source_points[0],
                                           wcs=wcs                    )
    y1, x1 = pixel_coordinates_at_position(image, source_points[1],
                                           wcs=wcs                    )
    d = math.sqrt((x0 - x1) ** 2 + (y0 - y1) ** 2)  # separation
    r0 = angle_to_pixel_diff(radius[0], wcs=wcs).value
    r1 = angle_to_pixel_diff(radius[1], wcs=wcs).value
    results['r0'] = r0
    results['r1'] = r1

    for key in 'F01', 'A01', 'off010', 'off011':
        results[key] = 0

    if d < (r0 + r1):
        # Find centre of overlapping region
        xc = x0 + ((d ** 2 + r0 ** 2 - r1 ** 2) / (2 * d ** 2) * (x1 - x0))
        yc = y0 + ((d ** 2 + r0 ** 2 - r1 ** 2) / (2 * d ** 2) * (y1 - y0))
        # Semi-major and -minor axes
        a = math.sqrt(r0 ** 2 - ((d ** 2 + r0 ** 2 - r1 ** 2)
                                 / (2 * d)                   ) ** 2)
        b = (r0 + r1 - d) / 2
        # Rotation angle
        theta = math.atan((x0 - x1) / (y1 - y0))
        theta = theta if theta >= 0 else theta + math.pi

        # Estimate flux by assuming as an ellipse
        results['F01'] = photutils.aperture_elliptical(image, xc, yc,
                                                       a, b, theta   )

        # Calculate area of overlapping region
        results['A01'] = (
                r0 ** 2 * math.acos((d ** 2 + r0 ** 2 - r1 ** 2)
                                    / (2 * d * r0)              )
                + r1 ** 2 * math.acos((d ** 2 - r0 ** 2 + r1 ** 2)
                                      / (2 * d * r1)              )
                - 0.5 *  math.sqrt((d + r0 + r1) * (-d + r0 + r1)
                                   * (d - r0 + r1) * (d + r0 - r1))
                          )

        eef = pacs_eef(kwargs.get('pacs_colour'), wcs)
        results['off010'] = ((eef(r0) - eef(d - r1)) * results['A01']
                             / (math.pi * (r0 ** 2 - (d - r1) ** 2)) )
        results['off011'] = ((eef(r1) - eef(d - r0)) * results['A01']
                             / (math.pi * (r1 ** 2 - (d - r0) ** 2)) )

    return results


def aperture_photometry_with_annulus(image, wcs, **kwargs):
    """
    The flux inside a circle corrected via an outer annulus
    """
    centre = kwargs.pop('centre', False)
    position = kwargs.pop('position', 'centre')
    radius = kwargs.pop('radius', None)
    scale = kwargs.pop('scale', 3)
    wcs = kwargs.pop('wcs', None)

    #               annulus_area * aperture_flux - aperture_area * annulus_flux
    # source_flux = -----------------------------------------------------------
    #               annulus_area * aperture_eef  - aperture_area * annulus_eef

    # annulus_eef = eef at outer edge - eef at inner edge of annulus
    # If no eef is given, then assume aperture_eef = 1 and annulus_eef = 0

    # radius needs to be a float number of pixels
    if radius is None:
        sigma = max(gauss_params[4:6])  # x, y sigmas from gaussfit
        radius = scale * sigma
    else:
        try:  # radius is an astropy.unit.quantity.Quantity (units=u.pix)
            radius = radius.value
        except AttributeError:  # float
            pass

    # Measure flux within so many sigma of gaussian centre
    aperture = {}
    circular_aperture = photutils.CircularAperture((x_c, y_c), radius)
    aperture['flux'] = photutils.aperture_photometry(image, circular_aperture)
    aperture['area'] = circular_aperture.area()

    # Annulus extends from 1.5 to 2 x radius of circular aperture
    annulus = {}
    annulus['inner'] = 1.5 * radius
    annulus['outer'] = 2. * radius
    annulus['flux'] = photutils.annulus_circular(image, x_c, y_c,
                                                 annulus['inner'],
                                                 annulus['outer'] )
    annulus['area'] = photutils.CircularAnnulus(annulus['inner'],
                                                annulus['outer']).area()

    # Get source energy fractions contained within aperture, annulus
    aperture['eef'], annulus['eef'] = aperture_annulus_eef(eef, radius,
                                                           annulus['inner'],
                                                           annulus['outer'] )

    # See top of function for explanation
    # Still needs HPF correction if a PACS map
    return ((annulus['area'] * aperture['flux']
             - aperture['area'] * annulus['flux'])
            / (annulus['area'] * aperture['eef']
               - aperture['area'] * annulus['eef']))


def background_mask(image, wcs, **kwargs):
    """
    Determine the pixels available for use as the background of an image
    """
    # Prepare array of background pixels
    # Generate array of pixel indices
    pixel_indices = np.vstack(np.dstack(np.indices(image.shape)))

    # Transform background_region to pixel indices
    background_region = pixel_coordinates_at_position(image,
                                            kwargs.get('background_region'),
                                                      wcs
                                                      )

    # Use points_inside_poly until Path.contains_points() is fixed
    import matplotlib.path
    path = matplotlib.path.Path(background_region, closed=True)
    mask = np.logical_not(path.contains_points(pixel_indices))
    #from matplotlib.nxutils import points_inside_poly
    #mask = np.logical_not(points_inside_poly(pixel_indices, background_region))

    # Combine this mask with specified points
    mask_points = pixel_coordinates_at_position(image,
                                                kwargs.get('mask_points'),
                                                wcs
                                                )
    mask_radius = angle_to_pixel_diff(kwargs.get('mask_radius', 20 * u.arcsec), wcs)
    for point in mask_points:
        mask = np.logical_or(mask,
                             ((pixel_indices[:, 1] - point[1]) ** 2
                              + (pixel_indices[:, 0] - point[0]) ** 2)
                             < mask_radius.value ** 2
                             )

    # Apply the mask
    return np.ma.array(image, mask=mask.reshape(image.shape))


def pacs_eef(pacs_colour, wcs):
    """
    Return the encircled energy fraction for a PACS scan-map point source.

    Parameters
    ----------
    pacs_colour : str, optional
        The PACS band to return the EEF for, options are:
            ``'red'``: 160um;
            ``'green'``: 100um;
            ``'blue'``: 70um;
        (default=``None``).
    wcs : astropy.wcs.WCS
        The world coordinate system for the image.

    Returns
    -------
    scipy.interpolate.UnivariateSpline :
        Takes parameter radius (in pixels), returning the eef at that radius.
    """
    import os
    from astropy import coordinates
    from astropy.io import ascii
    from astropy.table import Column, Table
    from scipy.interpolate import UnivariateSpline

    eef = ascii.read(os.path.dirname(os.path.realpath(__file__))
                     + '/pacs_aperture_correction.txt'          )

    # eef['radius'] is in arcsec - need to convert to pixels
    eef_radius = Column(name='radius', data=np.empty(len(eef)))
    for index, row in enumerate(eef):
        eef_radius[index] = angle_to_pixel_diff(
                coordinates.Angle(u.arcsec.to(u.degree, row['radius']),
                                  unit=u.degree),
                wcs                             ).value
    if pacs_colour not in ('red', 'green', 'blue'):
        print("pacs_colour must be one of 'red', 'green', 'blue'")
        return
    return UnivariateSpline(eef_radius, eef[pacs_colour], s=0)


def pacs_flux_correction(pacs_colour, wcs, radius, mask_radius, hpf_width):
    """ """
    radius = angle_to_pixel_diff(radius, wcs)
    pacs_correction = 1 / pacs_eef(pacs_colour, wcs)(radius.value)
    pacs_correction /= pacs_hpf_correction(pacs_colour, radius, mask_radius, hpf_width)
    return pacs_correction


def pacs_hpf_correction(pacs_colour, aperture_radius, mask_radius, hpf_width):
    """
    Return the high-pass filtering correction for a PACS scan-map point source.

    For 160um, hpf_width = 40, mask = 20", aperture = 20", correction = 0.96
    For 100um, hpf_width = 40, mask = 20", aperture = 20", correction = 0.98

    Returns
    -------
    scipy.interpolate.RectBivariateSpline :
        Takes parameters: radius (in degrees), hpf width (in readouts)
    """
    #if pacs_colour == 'red':  # For masking radius = 20", aperture = 20"
    #    pacs_correction = 0.96
    #elif pacs_colour == 'green':
    #    pacs_correction = 0.98
    #return pacs_correction

    # Read in values
    if pacs_colour == 'green':
        data = {4: astropy.io.ascii.read('hpf_effects_100_4.csv'),
                6: astropy.io.ascii.read('hpf_effects_100_6.csv'),
                8: astropy.io.ascii.read('hpf_effects_100_8.csv')}
        hpf_widths = np.array([10, 15, 20, 30, 40])
    else:
        data = {4: astropy.io.ascii.read('hpf_effects_160_4.csv'),
                6: astropy.io.ascii.read('hpf_effects_160_6.csv'),
                10: astropy.io.ascii.read('hpf_effects_160_10.csv')}
        hpf_widths = np.array([15, 20, 26, 30, 40])

    # Create arrays of points in aperture_radius, mask_radius, hpf_width space
    # and values at those points
    points = []
    values = []
    for mask_radius, table in data.items():
        for values_at_radius in table:
            for hpf_width in hpf_widths:
                points.append((
                    values_at_radius['aperture'],
                    mask_radius,
                    hpf_width,
                    ))
                values.append(values_at_radius[str(hpf_width) + '_readouts'])

    # Use a 1D spline to extend these lines to larger radii
    points_array = np.asarray(points)
    values_array = np.asarray(values)
    extra_points = []
    extra_values = []
    for m_radius in data.keys():
        for h_width in hpf_widths:
            single_line = scipy.interpolate.UnivariateSpline(
                points_array[np.logical_and(
                    points_array[:,1] == m_radius,
                    points_array[:,2] == h_width
                    )][:,0],
                values_array[np.logical_and(
                    points_array[:,1] == m_radius,
                    points_array[:,2] == h_width
                    )],
                k=2,
                )
            for ap_radius in range(points_array[-1][0] + 1, 41):
                extra_points.append((ap_radius, m_radius, h_width))
                extra_values.append(single_line(ap_radius))
    points += extra_points
    values += extra_values

    # Add 1s for all radii at very large patch sizes
    for m_radius in 25, 40:
        for ap_radius in range(2, 41):
            for h_width in hpf_widths:
                points.append((ap_radius, m_radius, h_width))
                values.append(1)
    points = np.asarray(points)
    values = np.asarray(values)

    # This data can now be interpolated
    fit = scipy.interpolate.SmoothBivariateSpline(
            x=points[points[:,0] == aperture_radius][:,1],  # mask radius
            y=points[points[:,0] == aperture_radius][:,2],  # hpf width
            z=values[points[:,0] == aperture_radius],  # values at those points
            #bbox=(4, 20, 5, 30),
            kx=2, ky=2,
            #s=2,
            )

    return fit(mask_radius, hpf_width)

    #aperture_radii = np.arange(2, 41)
    #mask_radii = np.array([4, 12, 20])
    #hpf_widths = np.array([10, 20, 30, 40])
    #data = np.ones((len(hpf_widths), len(mask_radii), len(aperture_radii)))
    #for aperture_index, aperture_radius in enumerate(aperture_radii):
    #
    #for width_index, hpf_width in enumerate(hpf_widths):
    #    for mask_index, mask_radius in enumerate(mask_radii):
    #        data[width_index, mask_index, aperture_index] = fit(
    #                mask_radius, hpf_width)


def pixel_coordinates_at_position(image, position, wcs=None):
    """
    Return the pixel coordinates of ``image`` corresponding to ``position``.

    Parameters
    ----------
    image : numpy.ndarray
        The image containing the coordinates
    position : str, astropy.coordinates.XXXCoordinates or container, optional
        A 2D Gaussian is used to fit the source, with initial guess position
        determined as a result of this parameter, options are:
        ``'centre'`` : The centre of the image (default)
        ``'maximum'`` : The position of maximum value in the image;
        XXXCoordinates : Coordinates.
        container : A list/tuple etc of XXXCoordinates objects.
    wcs : astropy.wcs.WCS, optional
        The world coordinate system of the image (default=``None``).

    Returns
    -------
    float :
        The y pixel coordinate
    float :
        The x pixel coordinate
    list : optional
        If ``position`` is a container, return a list of (float, float) tuples.
    """
    position_is_string = False
    try:
        if isinstance(position, basestring):
            position_is_string = True
    except NameError:
        if isinstance(position, str):
            position_is_string = True

    if position_is_string:
        if position == 'centre':
            y_pix, x_pix = int(image.shape[0] / 2), int(image.shape[1] / 2)
        elif position == 'maximum':
            import scipy.ndimage
            y_pix, x_pix = scipy.ndimage.measurements.maximum_position(image)
        else:
            print("Unknown position parameter: {0},".format(position),
                  "taken as 'centre'"                                 )
            y_pix, x_pix = int(image.shape[0] / 2), int(image.shape[1] / 2)
    else:
        # If position is an astropy.coordinates.XXXCoordinates object
        try:
            pixel_coords = wcs.wcs_world2pix([[position.ra.degree,
                                               position.dec.degree]],
                                               0                      )
            x_pix, y_pix = pixel_coords[0]
        except AttributeError:
            try:  # An iterable containing positions
                pixel_list = []
                for pos in position:
                    pixel_list.append(
                            pixel_coordinates_at_position(image, pos, wcs))
                return pixel_list
            except TypeError:  # A float / astropy.quantity.Quantity
                raise InnerError
            except InnerError:
                print("Assuming in pixels")
                try:
                    x_pix, y_pix = position[0].value, position[1].value
                except AttributeError:
                    x_pix, y_pix = position
    return y_pix, x_pix


def pixel_to_angle_diff(radius, wcs):
    """
    Convert a number of pixels to an angle given a WCS.

    Parameters
    ----------
    radius : float or astropy.units.quantity.Quantity
        A number of pixels representing a distance in an image.
    wcs : astropy.wcs.WCS
        A world coordinate system.

    Returns
    -------
    astropy.coordinates.angles.Angle :
        An angle equivalent to the input number of pixels.
    """
    from astropy.coordinates import Angle
    # degrees per pixel = wcs.wcs.cdelt[1]
    try:
        radius = radius.value
    except AttributeError:
        pass
    # radius is now a float number of pixels
    return Angle(radius * abs(wcs.wcs.cdelt[1]), unit=u.degree)


def random_aperture_photometry(image, wcs, **kwargs):
    """
    """
    hpf_width = kwargs.pop('hpf_width', None)
    pacs_colour = kwargs.pop('pacs_colour', None)
    pacs = kwargs.pop('pacs', False)
    radius = kwargs.pop('radius', None)
    random_aperture_positions = kwargs.pop('random_aperture_positions', None)
    source_position = kwargs.pop('source_position', 'centre')

    source_flux = aperture_flux(image, #centre=True,
                                position=source_position,
                                radius=radius,
                                wcs=wcs,
                                )

    # Calculate flux in each random aperture
    random_fluxes = []
    for aperture_position in random_aperture_positions:
        random_fluxes.append(aperture_flux(image, centre=False,
                                           position=aperture_position,
                                           radius=radius,
                                           wcs=wcs,
                                           ))
    background_flux = np.mean(random_fluxes)
    source_flux_std = np.std(random_fluxes)

    source_flux -= background_flux

    radius = angle_to_pixel_diff(radius, wcs)

    if pacs:
        pacs_correction = 1 / pacs_eef(pacs_colour, wcs)(radius.value)
        if pacs_colour == 'red':  # For masking radius = 20", aperture = 20"
            pacs_correction /= 0.96
        elif pacs_colour == 'green':
            pacs_correction /= 0.98
        #correction /= pacs_hpf_correction(pacs_colour)(radius.degree,
        #                                               hpf_width      )[0][0]
        source_flux *= pacs_correction

    return source_flux, source_flux_std


def random_aperture_photometry_bootstrap(image, wcs, **kwargs):
    """
    """
    results = aperture_flux(image, wcs=wcs, area=True, **kwargs)
    pacs_colour = kwargs.get('pacs_colour', 'red')
    masked_image = background_mask(image, wcs, **kwargs)

    # results can be a (flux, area) tuple, or dictionary
    try:
        source_flux, source_area = results
        # bootstrapped random aperture fluxes
        # requires numpy 1.7
        b0 = mathsu.bootstrap(masked_image, int(source_area), np.sum)
        B0 = np.mean(b0)
        source_flux_std = np.std(b0)
        source_flux -= B0
        source_flux *= pacs_flux_correction(pacs_colour, wcs, **kwargs)
        return source_flux, source_flux_std

    except ValueError:  # dictionary due to multiple central sources
        # Labelling sources as 0, 1
        # measured flux = F, background = B, area = A, true flux = f
        # overlapping region = 01
        b0 = mathsu.bootstrap(masked_image, int(results['A0']), np.sum)
        b1 = mathsu.bootstrap(masked_image, int(results['A1']), np.sum)
        b01 = mathsu.bootstrap(masked_image, int(results['A01']), np.sum)
        B0 = np.mean(b0)
        std_B0 = np.std(b0)
        B1 = np.mean(b1)
        std_B1 = np.std(b1)
        B01 = np.mean(b01)
        if len(b01) == 1:
            B01 = 0
        eef = pacs_eef(pacs_colour, wcs)
        f0 = (((results['F0'] - B0) - (results['F01'] - B01))
              / (eef(results['r0']) - results['off010'])     )
        f1 = (((results['F1'] - B1) - (results['F01'] - B01))
              / (eef(results['r1']) - results['off011'])     )
        f0 /= pacs_hpf_correction(pacs_colour)
        f1 /= pacs_hpf_correction(pacs_colour)
        return f0, std_B0, f1, std_B1


def source_flux_from_fits(path, **kwargs):
    """
    Calculate the flux from a source in a FITS file.

    Parameters
    ----------
    path: str
        The path to the FITS file
    eef : astropy.table.Table or numpy.recarray, optional
        Encircled energy fraction/function: a table of the fractional flux of a
        point source enclosed within a given pixel radius.
        Columns ``radius`` and ``fraction``.
    hpf_width : int
        If the file is a PACS scan map, how many readouts is the high-pass
        filtering?
    pacs : bool, optional
        Is the file a PACS scan map? Uses a built-in aperture correction (EED)
        if ``True``, (default=``False``).
    pacs_colour : str, optional
        if ``pacs=True``, which band is used, options are:
            ``'red'``: 160um;
            ``'green'``: 100um;
            ``'blue'``: 70um;
        (default=``None``).
    position : str, astropy.coordinates.XXXCoordinates, optional
        A 2D Gaussian is used to fit the source, with initial guess position
        determined as a result of this parameter, options are:
        ``'centre'`` : The centre of the image (default)
        ``'maximum'`` : The position of maximum value in the image;
        XXXCoordinates : Coordinates.
    radius : astropy.units.quantity.Quantity, optional
        Use a specific radius (in number of pixels) for the aperture as opposed
        to ``scale * sigma``, (default=``0``).
    scale : float, optional
        The circular aperture placed over the source has radius
        ``scale * sigma``, where ``sigma`` is from the Gaussian fit
        (default=``6``).
        The surrounding annulus has inner and outer radii of ``1.5``,
        ``2 * scale * sigma``.

    Returns
    -------
    float :
        Source flux in units of those used in the input file
    """
    hpf_width = kwargs.pop('hpf_width', None)
    pacs_colour = kwargs.pop('pacs_colour', None)
    pacs = kwargs.pop('pacs', False)
    radius = kwargs.pop('radius', None)
    random_aperture_positions = kwargs.pop('random_aperture_positions', None)
    scale = kwargs.pop('scale', 3)
    source_position = kwargs.pop('source_position', 'centre')

    from astropy.io import fits
    from astropy import wcs

    image = fits.open(path)[1]
    image.verify('silentfix')
    image_wcs = wcs.WCS(image.header)
    image = image.data

    if random_aperture_positions:
        return random_aperture_photometry(image, image_wcs,
                                          hpf_width=hpf_width,
                                          pacs_colour=pacs_colour,
                                          pacs=pacs,
                                          radius=radius,
                                          random_aperture_positions=\
                                               random_aperture_positions,
                                          source_position=source_position,
                                          )
    else:
        return aperture_photometry_with_annulus(image,
                                                radius=radius,
                                                scale=scale,
                                                source_position=\
                                                    source_position,
                                                wcs=image_wcs)


def source_flux_from_fits_bootstrap(path, **kwargs):
    """
    Calculate the flux from a source in a FITS file.

    Parameters
    ----------
    path: str
        The path to the FITS file
    eef : astropy.table.Table or numpy.recarray, optional
        Encircled energy fraction/function: a table of the fractional flux of a
        point source enclosed within a given pixel radius.
        Columns ``radius`` and ``fraction``.
    hpf_width : int
        If the file is a PACS scan map, how many readouts is the high-pass
        filtering?
    mask_radius: int
        If the file is a PACS scan map, how large was the radius of the source
        mask patch?
    pacs : bool, optional
        Is the file a PACS scan map? Uses a built-in aperture correction (EED)
        if ``True``, (default=``False``).
    pacs_colour : str, optional
        if ``pacs=True``, which band is used, options are:
            ``'red'``: 160um;
            ``'green'``: 100um;
            ``'blue'``: 70um;
        (default=``None``).
    position : str, astropy.coordinates.XXXCoordinates, optional
        A 2D Gaussian is used to fit the source, with initial guess position
        determined as a result of this parameter, options are:
        ``'centre'`` : The centre of the image (default)
        ``'maximum'`` : The position of maximum value in the image;
        XXXCoordinates : Coordinates.
    radius : astropy.units.quantity.Quantity, optional
        Use a specific radius (in number of pixels) for the aperture as opposed
        to ``scale * sigma``, (default=``0``).
    scale : float, optional
        The circular aperture placed over the source has radius
        ``scale * sigma``, where ``sigma`` is from the Gaussian fit
        (default=``6``).
        The surrounding annulus has inner and outer radii of ``1.5``,
        ``2 * scale * sigma``.

    Returns
    -------
    float :
        Source flux in units of those used in the input file
    """

    from astropy.io import fits
    from astropy import wcs

    image = fits.open(path)[1]
    image.verify('silentfix')
    image_wcs = wcs.WCS(image.header)
    image = image.data

    if kwargs.get('background_region'):
        return random_aperture_photometry_bootstrap(image, image_wcs, **kwargs)
    else:
        return aperture_photometry_with_annulus(image, image_wcs, **kwargs)
