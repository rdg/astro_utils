"""
This module contains useful functions that deal with plotting spectra,
spectral lines, dual x scales etc
"""
from __future__ import print_function, division

import matplotlib
matplotlib.rcParams.update({
    'axes.linewidth': 0.5,
    'xtick.major.size': 4,
    'xtick.minor.size': 2,
    'ytick.major.size': 4,
    'ytick.minor.size': 2,
    'font.family': 'serif',
    'font.size': 10,
    'text.usetex': True,
    'text.latex.preamble': [
            r'\usepackage{mathptmx}',
            r'\usepackage[T1]{fontenc}',
            r'\usepackage{bm}',
            r'\usepackage{amsmath}',
            r'\usepackage{upgreek}',
            r'\usepackage{textcomp}'
            r'\usepackage{siunitx}',
            r'\sisetup{range-units=single,'
            r'        range-phrase = \,--\,,'
            r'        list-units=single,'
            r'        number-unit-product = \,,'
            r'        separate-uncertainty = true,'
            r'        multi-part-units = single,'
            r'        detect-weight,'
            r'        detect-display-math,'
            r'        detect-inline-weight=math,'
            r'        }',
            r'\DeclareSIUnit \yr {yr}',
            r'\DeclareSIUnit \myr {\mega \yr}',
            r'\DeclareSIUnit \gyr {\giga \yr}',
            r'\DeclareSIUnit \h {\ensuremath{h}}',
            r'\DeclareSIUnit \jansky {Jy}',
            r'\DeclareSIUnit \Jy {\jansky}',
            r'\DeclareSIUnit \mJy {\milli \jansky}',
            r'\DeclareSIUnit \uJy {\micro \jansky}',
            r'\DeclareSIUnit \parsec {pc}',
            r'\DeclareSIUnit \pc {\parsec}',
            r'\DeclareSIUnit \kpc {\kilo \pc}',
            r'\DeclareSIUnit \Mpc {\mega \pc}',
            r'\DeclareSIUnit \kms {\kilo\metre\per\second}',
            r'\DeclareSIUnit \arcs {''}',
            r'\DeclareSIUnit \degsq {deg\ensuremath{^2}}',
            r'\DeclareSIUnit \lsun {\ensuremath{L_\odot}}',
            r'\DeclareSIUnit \msun {\ensuremath{M_\odot}}',
            r'\DeclareSIUnit \zsun {\ensuremath{Z_\odot}}',
            r'\DeclareSIUnit \mstar {\ensuremath{M^*}}',
            ],
    'pgf.texsystem': 'lualatex',
    'pgf.rcfonts': False,
    'pgf.preamble': [
            r'\usepackage{amsmath}',
            r'\usepackage{bm}',
            r'\renewcommand{\v}{\bm}',
            r'\usepackage{lualatex-math}',
            r'\usepackage{unicode-math}',  # unicode math setup
            r'\setmathfont{XITS Math}',
            r'\setmainfont{texgyretermes}',
            r'\usepackage{siunitx}',
            r'\sisetup{range-units=single,'
            r'        range-phrase = \,--\,,'
            r'        list-units=single,'
            r'        number-unit-product = \,,'
            r'        separate-uncertainty = true,'
            r'        multi-part-units = single,'
            r'        detect-weight,'
            r'        detect-display-math,'
            r'        detect-inline-weight=math,'
            r'        }',
            r'\DeclareSIUnit \yr {yr}',
            r'\DeclareSIUnit \myr {\mega \yr}',
            r'\DeclareSIUnit \gyr {\giga \yr}',
            r'\DeclareSIUnit \h {\ensuremath{h}}',
            r'\DeclareSIUnit \jansky {Jy}',
            r'\DeclareSIUnit \Jy {\jansky}',
            r'\DeclareSIUnit \mJy {\milli \jansky}',
            r'\DeclareSIUnit \ujy {\micro \jansky}',
            r'\DeclareSIUnit \Ghz {\GHz}',
            r'\DeclareSIUnit \parsec {pc}',
            r'\DeclareSIUnit \pc {\parsec}',
            r'\DeclareSIUnit \kpc {\kilo \pc}',
            r'\DeclareSIUnit \Mpc {\mega \pc}',
            r'\DeclareSIUnit \kms {\kilo\metre\per\second}',
            r'\DeclareSIUnit \arcs {''}',
            r'\DeclareSIUnit \degsq {deg\squared}',
            r'\DeclareSIUnit \lsun {\ensuremath{L_\odot}}',
            r'\DeclareSIUnit \msun {\ensuremath{M_\odot}}',
            r'\DeclareSIUnit \zsun {\ensuremath{Z_\odot}}',
            r'\DeclareSIUnit \mstar {\ensuremath{M^*}}',
            ],
    'ps.usedistiller': 'xpdf',
    })
import matplotlib.figure
from matplotlib.transforms import Transform, blended_transform_factory
import numpy as np


class UpperToLowerTransform(Transform):
    r"""
    A generic matplotlib axis transformation, example code is below:

    import astro_utils.plot_utils as plotu
    import astro_utils.spec_utils as specu


    # Get and prepare data
    x_data = None
    y_data = None

    fig = plotu.create_figure()
    fig.plot(x_data, y_data)
    axes_lower = fig.axes[0]
    #                       lower_to_upper_function, upper_to_lower_function
    axes_upper = fig.add_upper_scale(specu.ghz_to_um, specu.um_to_ghz)

    # Save the figure
    fig.savefig('line.pdf')

    Initialising arguments
    ----------------------
    upper_to_lower_function : function
        Convert an upper axis value into a lower
    lower_to_upper_function : function
        Convert a lower axis value into an upper
    """

    input_dims = 1  # input_dimensions - subclasses need to override
    output_dims = 1
    is_separable = False  # ?
    has_inverse = True

    def __init__(self, upper_to_lower_function, lower_to_upper_function):
        """
        Calculate the lower axis values corresponding to upper axis values

        Overwrites that of Transform and thus TransformNode

        Parameters
        ----------
        upper_to_lower_function : function
            Convert an upper axis value into a lower one
        lower_to_upper_function : function
            Convert a lower axis value into an upper one
        """
        # Copy the __init__ from TransformNode (parent of Transform)

        # Parents are stored in a WeakValueDictionary, so that if the
        # parents are deleted, references from the children won't keep
        # them alive.
        from weakref import WeakValueDictionary
        self._parents = WeakValueDictionary()
        # TransformNodes start out as invalid until their values are
        # computed for the first time.
        self._invalid = 1

        # Allows the use of input functions to transform between axes
        # If inputs are ``None``, act as IdentityTransform()
        if upper_to_lower_function is None or lower_to_upper_function is None:
            upper_to_lower_function = lower_to_upper_function = lambda x: x
        self.upper_to_lower_function = upper_to_lower_function
        self.lower_to_upper_function = lower_to_upper_function

    def transform_non_affine(self, upper_axis_value):
        """
        Calculate the lower axis value equivalent to the input upper value

        Parameters
        ----------
        upper_axis_value : ndarray containing a single float
            The upper axis value to convert

        Returns
        -------
        ndarray :
            The corresponding lower axis value
        """
        return self.upper_to_lower_function(upper_axis_value)

    def inverted(self):
        """ Perform the lower axis value to upper axis value transformation
        """
        return _LowerToUpperTransform(self.upper_to_lower_function,
                                      self.lower_to_upper_function)


class _LowerToUpperTransform(UpperToLowerTransform):
    """
    Called by UpperToLowerTransform, can just ignore
    """

    def transform_non_affine(self, lower_axis_value):
        """
        Calculate the upper axis value equivalent to the input lower value

        Parameters
        ----------
        lower_axis_value : ndarray containing a single float
            The lower axis value to convert

        Returns
        -------
        ndarray :
            The corresponding upper axis value
        """
        return self.lower_to_upper_function(lower_axis_value)

    def inverted(self):
        """
        Perform the upper axis value to lower axis value transformation
        """
        return UpperToLowerTransform(self.upper_to_lower_function,
                                     self.lower_to_upper_function)


def add_spectral_lines(self, font_size=8, disp_type='wavelength', redshift=0):
    """
    Add spectral lines to the current axes (specified in spectral_lines.txt).

    Requires ``lineid_plot``

    Parameters
    ----------
    font_size : int?, optional
        The text size of the labels added (default=``10``).
    disp_type : str, optional
        The unit type of the dispersion of the current axes:
            ``'wavelength'`` (default)
            ``'frequency'``
    redshift : float, optional
        The redshift of the spectrum (default=``0``)
    """
    try:
        axes = self.gca()
    except AttributeError:
        axes = self
    dispersion = axes.get_lines()[0].get_xdata()
    flux = axes.get_lines()[0].get_ydata()

    # Create ``lines``, a list of the spectral lines within range
    import astro_utils.spec_utils as spec_utils
    lines = spec_utils.lines_in_range(
                spec_utils.blueshift(dispersion[0], redshift, disp_type),
                spec_utils.blueshift(dispersion[-1], redshift, disp_type),
                                      disp_type)
    if disp_type == 'frequency':
        line_dispersions = [spec_utils.redshift(spec_utils.um_to_ghz(line[0]),
                                                redshift                      )
                            for line in lines]
    else:
        line_dispersions = [spec_utils.redshift(line[0], redshift,
                                                disp_type='wavelength')
                            for line in lines]
    line_labels = [r'{0}{2}{1}'.format(line[1], line[2], r'\hspace{0.2cm}')
                   for line in lines]

    # Determine the label sizes
    from matplotlib.textpath import TextPath
    for i, line in enumerate(lines):
        line.append((TextPath((0, 0), line_labels[i],
                              size=font_size         ).get_extents().width)
                    / 2.5)  # Scaling factor?

    # Rescale the plot to allow room for line labels
    max_flux = max(flux)
    y_display_max = axes.transData.transform((0, max_flux))[1]
    y_display_max += max([line[3] for line in lines])
    y_data_max = axes.transData.inverted().transform((0, y_display_max))[1]
    axes.set_ylim(None, y_data_max)
    upper_space = y_data_max - max_flux

    # Plot the lines and labels
    import lineid_plot
    lineid_plot.plot_line_ids(dispersion, flux, line_dispersions,
                              line_labels, ax=axes, label1_size=font_size,
                              arrow_tip=(max_flux + 0.15 * upper_space),
                              box_loc=(max_flux + 0.55 * upper_space)     )

# Add add_spectral_lines to matplotlib.figure.Figure,
#                           matplotlib.axes.Axes      methods
matplotlib.figure.Figure.add_spectral_lines = add_spectral_lines
matplotlib.axes.Axes.add_spectral_lines = add_spectral_lines


def add_two_scale_axes(self, axes_spec, **kwargs):
    """
    Return an Axes instance added to self, with ability to `add_upper_scale`.
    """
    from mpl_toolkits.axes_grid1.parasite_axes import HostAxes, SubplotHost
    try:
        return self.add_axes(HostAxes(self, axes_spec, **kwargs))
    except TypeError:
        return self.add_subplot(SubplotHost(self, axes_spec, **kwargs))

matplotlib.figure.Figure.add_two_scale_axes = add_two_scale_axes


def add_upper_scale(self, lower_to_upper_transform, upper_to_lower_transform,
                    left_to_right_transform=None,
                    right_to_left_transform=None                             ):
    """
    Return an axes instance with a different x-axis scale defined by inputs.

    The parameter functions define axis value tranformations between the lower
    x-axis and the upper.

    Parameters
    ----------
    lower_to_upper_transform : function : 1 parameter (float) returning 1 float
        A function converting a value on the lower x-axis into the
        corresponding value on the upper x-axis
    upper_to_lower_transform : function : 1 parameter (float) returning 1 float
        A function converting a value on the upper x-axis into the
        corresponding value on the lower x-axis

    Returns
    -------
    mpl_toolkits.axes_grid1.parasite_axes :
        A parasite axes overlaying that called on, with an x-axis scale related
        to the host by the transform functions.

    """
    try:
        axes = self.gca()
    except AttributeError:
        axes = self

    # Create and apply transform to twin axis
    twin_axes_transform = blended_transform_factory(
            UpperToLowerTransform(upper_to_lower_transform,
                                  lower_to_upper_transform ),
            UpperToLowerTransform(right_to_left_transform,
                                  left_to_right_transform )
                                                    )
    try:
        axes_upper = axes.twin(twin_axes_transform)
    except AttributeError:  # Need to create using parasite_axes.SubplotHost
        print("Use astro_utils.plot_utils.create_figure or add the host axes"
              " via mpl_toolkits.axes_grid1.parasite_axes.SubplotHost to add"
              " an upper axis scale to the plot"                             )
        return
    axes_upper.set_viewlim_mode('transform')

    axes.xaxis.set_ticks_position('bottom')
    axes_upper.xaxis.set_ticks_position('top')
    axes.yaxis.set_ticks_position('left')
    axes_upper.yaxis.set_ticks_position('right')

    return axes_upper

# Add add_upper_scale to matplotlib.figure.Figure, matplotlib.axes.Axes methods
matplotlib.figure.Figure.add_upper_scale = add_upper_scale
matplotlib.axes.Axes.add_upper_scale = add_upper_scale


def array_steps(a, is_x=False):
    """
    Return a new array twice the length of the input for plotting as steps.

    Dispersion (x) arrays e.g. ``([x1, x2])`` are returned as
    ``([x1 - d, x1 + d, x2 - d, x2 + d])``, where ``d = (x2 - x1) / 2``.
    Other arrays e.g. ``([y1, y2]) are returned as ``([y1, y1, y2, y2])``.

    Parameters
    ----------
    a : array_like
        The data to prepare for step plotting.
    is_x : bool, optional
        Does the array specify dispersion (x) values for the plot?
        (default=``False``).

    Returns
    -------
    ndarray :
        The stepped version of ``a``.
    """
    a = np.asarray(a)
    if is_x:
        stepped_array = np.hstack((
            [1.5 * a[0] - 0.5 * a[1]],
            np.repeat((a[:-1] + a[1:]) / 2, 2),
            [1.5 * a[-1] - 0.5 * a[-2]]
            ))
    else:
        stepped_array = np.repeat(a, 2)
    return stepped_array


def create_figure(**kwargs):
    r"""
    Create a matplotlib.figure.Figure with LaTeX text, optional two-scale axes.

    Parameters
    ----------
    font : string, optional
        The latex font to plot labels in (default=``\usepackage{mathptmx}``).
    font_size : int?, optional
        The size of font to plot labels in (default=``16``).
    axes_spec : Initialising parameter of matplotlib.figure.Figure.add_axes or
                matplotlib.figure.Figure.add_subplot, optional
        An existing Axes instance, or placement object e.g. ``(l, b, w, h)``
        or matplotlib.gridspec.SubplotSpec, ``None`` will result in no Axes
        being added, (default=``'111'``).
    Any kwarg to pass to matplotlib.figure.Figure, optional

    Returns
    -------
    matplotlib.figure.Figure :
        The created figure.
    """
    font = kwargs.pop('font', r'\usepackage{mathptmx}')
    font_size = kwargs.pop('font_size', 10)
    axes_spec = kwargs.pop('axes_spec', '111')

    # Set up the latex text
    matplotlib.rcParams.update({
        'font.size': font_size,
        #'text.latex.preamble': [font],
        })


    # Set up the plot
    import matplotlib.pyplot as plt
    fig = plt.figure(**kwargs)

    if axes_spec is not None:
        fig.add_two_scale_axes(axes_spec)

    return fig


def fill_between_colours(self, x, y_low, y_high, colours, linewidth=0):
    """
    fill_between, but colours the fill using the passed array of colours.

    Parameters
    ----------
    x : array_like
         As in fill_between
    y_low : array_like
         As in fill_between
    y_high : array_like
         As in fill_between
    colours : array_like
        [[R, G, B {, A}], ...]
    linewidth : float
        The width of the lines bounding the colour segments.
        Set to 0.5 or so to smooth the segments

    Returns
    -------
    Same as fill_between
    """
    len_colours = len(colours)
    for index, colour in enumerate(colours):
        # Arrays describing patch will contain
        # start, any indices crossed, end

        # Find start x of colour patch (plus surrounding indices)
        x_start = (x[-1] - x[0]) * index / len_colours + x[0]
        index_after_start = np.searchsorted(x, x_start)
        index_before_start = index_after_start - 1

        # Find end x of colour patch (plus surrounding indices)
        x_end = (x[-1] - x[0]) * (index + 1) / len_colours + x[0]
        index_after_end = np.searchsorted(x, x_end)
        index_before_end = index_after_end - 1

        # These patches will probably be pretty much transparent anyway
        if index_before_start < 0 or index_after_end > (len(x) -1):
            continue

        # Find any x indices crossed in the x values for this patch
        index_cut = slice(index_after_start, index_before_end + 1)

        # Create the final x array describing this patch
        x_cut = [x_start] + list(x[index_cut]) + [x_end]

        # Get the final y_high array describing this patch
        try:
            y_high_m = ((y_high[index_after_start]
                                - y_high[index_before_start])
                        / (x[index_after_start] - x[index_before_start]))
            y_high_start = (y_high[index_before_start]
                            + y_high_m * (x_start - x[index_before_start]))
            y_high_m = ((y_high[index_after_end] - y_high[index_before_end])
                        / (x[index_after_end] - x[index_before_end]))
            y_high_end = (y_high[index_before_end]
                            + y_high_m * (x_end - x[index_before_end]))
            y_high_cut = [y_high_start] + list(y_high[index_cut]) + [y_high_end]
        except TypeError:  # y_high is a scalar
            y_high_cut = y_high

        # Get the final y_low array describing this patch
        try:
            y_low_m = ((y_low[index_after_start] - y_low[index_before_start])
                        / (x[index_after_start] - x[index_before_start]))
            y_low_start = (y_low[index_before_start]
                           + y_low_m * (x_start - x[index_before_start]))
            y_low_m = ((y_low[index_after_end] - y_low[index_before_end])
                        / (x[index_after_end] - x[index_before_end]))
            y_low_end = (y_low[index_before_end]
                           + y_low_m * (x_end - x[index_before_end]))
            y_low_cut = [y_low_start] + list(y_low[index_cut]) + [y_low_end]
        except TypeError:  # y_low is a scalar
            y_low_cut = y_low

        # Add the patch to the axes
        self.fill_between(x_cut,
                          y_low_cut, y_high_cut,
                          facecolor=colour,
                          edgecolor=colour, linewidth=linewidth,
                          )

matplotlib.axes.Axes.fill_between_colours = fill_between_colours


def plot_step_fill(self, x_data, y_data, **kwargs):
    """
    Plot data as steps, filled with colour between ``y_data`` and 0.

    Parameters
    ----------
    x_data : array_like
        The points specifying x values e.g. dispersion.
    y_data : array_like
        The points specifying y values e.g. flux
    fillcolour : matplotlib colour (see `matplotlib.colors`), optional
        An object specifying a matplotlib color (hex value, RGB tuple etc),
        (default=``(1, 1, 0)`` (yellow)).
    linewidth : float, optional
        Of stepped line, (default=``1``).
    colour_linewidth : float, optional
        Linewidth for colour patches (set to 0.5 to smooth gradients)
    hline : bool, optional
        Plot a dashed line along y=0, linewidth=``0.5 * linewidth``,
        (default=``True``).
    """
    fillcolour = kwargs.get('fillcolour', "yellow")
    linecolour = kwargs.pop('linecolour', "black")  # Black
    linewidth = kwargs.get('linewidth', 0)
    colour_linewidth = kwargs.get('colour_linewidth', 0)
    hline = kwargs.get('hline', True)
    y_fill_to = kwargs.get('y_fill_to', 0)
    clip_on = kwargs.get('clip_on', True)
    # Create a figure if one has not been passed in
    try:
        axes = self.gca()
    except AttributeError:
        axes = self

    # Plot fill under spectrum
    if fillcolour is not None:
        try:
            if type(fillcolour) is not str:
                fillcolour[0][0]
                axes.fill_between_colours(array_steps(x_data, is_x=True),  # x
                                          array_steps(y_data),             # y_1
                                          y_fill_to,                       # y_2
                                          fillcolour,
                                          linewidth=colour_linewidth,
                                          clip_on=clip_on,
                                          )
            else:
                raise TypeError
        except TypeError:
            x = array_steps(x_data, is_x=True)
            y = array_steps(y_data)
            axes.fill_between(x=array_steps(x_data, is_x=True),  # x
                              y1=array_steps(y_data),            # y_1
                              y2=y_fill_to,                      # y_2
                              facecolor=fillcolour,
                              edgecolor=linecolour, linewidth=colour_linewidth,
                              clip_on=clip_on,
                              )

    # Plot dashed line at y = 0
    if hline:
        axes.axhline(linestyle='--', color='black',
                     linewidth=max(0.5, 0.5 * linewidth),
                     clip_on=clip_on,
                     )

    # Plot stepped spectrum
    return axes.plot(x_data, y_data, linestyle='steps-mid',
                     color=linecolour, linewidth=linewidth,
                     clip_on=clip_on,
                     )

# Add plot_step_fill to matplotlib.figure.Figure, matplotlib.axes.Axes methods
matplotlib.figure.Figure.plot_step_fill = plot_step_fill
matplotlib.axes.Axes.plot_step_fill = plot_step_fill


def set_ticklabels_pad(self, pad):
    """
    Set the padding (in points) for the axis tick labels

    Parameters
    ----------
    pad : float
        The padding in points
    """
    for tick in self.get_major_ticks():
        tick.set_pad(pad)
        tick.label1 = tick._get_text1()

matplotlib.axis.Axis.set_ticklabels_pad = set_ticklabels_pad
matplotlib.axis.Axis.set_ticklabelpad = set_ticklabels_pad
matplotlib.axis.Axis.set_ticklabelspad = set_ticklabels_pad

