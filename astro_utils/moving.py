"""
Moving Functions
================

A moving average is a calculation of the average at each element within an
array, determined from the surrounding elements.  ``numpy`` provides no
built-in methods for calculation of moving functions.

This module implements::

    moving_mean
    moving_median
    moving_std

The ``scipy.signal`` window functions can be used to weight the surrounding
elements, and are optional parameters.
"""

import numpy as np


def _extend_array(a, size=None):
    """
    Return an array extended with ``int(size / 2)`` ``np.nan`` values.

    Parameters
    ----------
    a : array_like
        Input data to generate moving windows from.
    size : number
        Size of window in number of elements of ``a``.

    Returns
    -------
    numpy.ndarray
        array with ``np.nan`` values concatenated before and after ``a``.
    """
    a = np.asarray(a)
    # Add np.nan elements to both ends of array (currently 1d)
    extent = int(size / 2)

    # Construct a single column of np.nan elements of the right dimensions
    try:
        nans = np.reshape([np.nan] * np.product(a.shape[:-1]), a.shape[:-1])
    except TypeError:  # If a is 1d then have problems
        nans = np.array([np.nan])

    # Repeat this column the required number of times and add to each end of a
    try:
        return np.hstack((np.tile(nans, extent), a,
                          np.tile(nans, size - extent - 1)))
    except ValueError:  # Require a different stacking function for 3d - why?
        return np.dstack((np.tile(nans, extent), a,
                          np.tile(nans, size - extent - 1)))


def _moving_windows(a, size=None, window='boxcar', window_args=()):
    """
    Return an array of ``a`` slices of width ``size`` and function ``window``.

    This is slower than the corresponding ``bottleneck`` function
    (``move_mean`` etc) but ``np.nan`` values are appended at the ends to fill
    necessary windows, which is not performed in ``bottleneck``, and allows a
    choice of window function.

    Parameters
    ----------
    a : array_like
        Input data to generate moving windows from.
    size : int
        Size of window in number of elements of ``a``.
    window : str, optional
        Window function to use, see ``scipy.signal`` for available functions,
        (default = ``'boxcar'``).
    window_args : tuple, optional
        Any arguments required for the window function e.g. sigma for
        ``window='gaussian'``, (default = ``()``).

    Returns
    -------
    numpy.ndarray
        An array of slices of ``a`` of width ``window``.
    """
    # Generate the array of slices
    a_extended = _extend_array(a, size)
    shape = a_extended.shape[:-1] + (a_extended.shape[-1] - size + 1, size)
    strides = a_extended.strides + (a_extended.strides[-1],)
    a_windows = np.lib.stride_tricks.as_strided(a_extended,
                                                shape=shape, strides=strides)
    # Get (normalised * size) window function and multiply a_windows by it
    import scipy.signal
    window = scipy.signal.get_window((window,) + tuple(window_args), size)

    # Need to return  a_windows * window * size / np.sum(window),
    # but size depends on number of np.nan values in each a_window
    # Construct mask of np.nan values
    a_masked = np.ma.masked_array(a_windows, np.isnan(a_windows))
    # Construct normalisation array such that
    # (sum(window[a_window not isnan]) = len(a_window not isnan))
    norm = a_masked.count(axis=-1) \
                    / np.sum(window * np.logical_not(a_masked.mask), axis=-1)
    # Reshape norm and return the final normalised array
    return a_windows * window * np.reshape(norm, (len(a_windows), 1))


def bin_array(a, bin_width):
    """
    """
    shape = (a.shape[-1] // bin_width, bin_width)
    strides = (a.strides[0] * bin_width,) + (a.strides[-1],)
    a_bins = np.lib.stride_tricks.as_strided(a, shape=shape, strides=strides)
    return np.mean(a_bins, axis=1)


def moving_mean(a, size=None, window='boxcar', window_args=()):
    """
    Return the moving mean at each ``a`` element across a width ``window``.

    Similar to ``bottleneck.move_mean`` but continues to the end elements and
    allows a choice of window function.
    For one call, performance is similar with and without ``bottleneck``
    installed. For many calls, performance increases by ~3x for a generic
    window, and ~50x for ``window='boxcar'`` with ``bottleneck`` installed.

    Parameters
    ----------
    a : array_like
        Input data to generate moving windows from.
    size : int, optional
        Size of window in number of elements of ``a`` (default=``len(a)//4``).
    window : str, optional
        Window function to use, see ``scipy.signal`` for available functions,
        (default = ``'boxcar'``).
    window_args : tuple, optional
        Any arguments required for the window function e.g. sigma for
        ``window='gaussian'``, (default = ``()``).

    Returns
    -------
    numpy.ndarray
        Array of size ``a.size`` containing the moving mean of ``a``.
    """
    if size is None:
        size = len(a) // 4
    try:
        import bottleneck as bn
        #if window == 'boxcar':
        #    # A faster method but only for boxcar windows
        #    # Hopefully this gets the ends correct
        #    a_extended = _extend_array(a, size)
        #    moving = bn.move_mean(a_extended, size, axis=-1)
        #    return moving[np.logical_not(np.isnan(moving))]
        # Otherwise have to use general window function code
        a_windows = _moving_windows(a, size, window, window_args)
        return bn.nanmean(a_windows, axis=-1)

    except ImportError:
        # This method is much slower
        a_windows = _moving_windows(a, size, window, window_args)
        # Need to mask np.nan within each window
        a_masked = np.ma.masked_array(a_windows, np.isnan(a_windows))
        return np.ma.mean(a_masked, axis=-1).data


def moving_median(a, size=None):
    """
    Return the moving median at each ``a`` element across a width ``window``.

    Similar to ``bottleneck.move_median`` but continues to the end elements.
    With ``bottleneck`` installed, performance increases by ~3x.

    Parameters
    ----------
    a : array_like
        Input data to generate moving windows from.
    size : int
        Size of window in number of elements of ``a``.

    Returns
    -------
    numpy.ndarray
        Array of size ``a.size`` containing the moving median of ``a``.
    """
    if size is None:
        size = len(a) // 4
    # Get the window slices of a
    a_windows = _moving_windows(a, size, window='boxcar')

    # Calculate the median value within each window
    try:
        import bottleneck as bn
        return bn.nanmedian(a_windows, axis=-1)  # No bn.move_nanmedian yet
    except ImportError:
        # This method is much slower
        # Need to mask np.nan within each window
        a_masked = np.ma.masked_array(a_windows, np.isnan(a_windows))
        return np.ma.median(a_masked, -1).data


def moving_std(a, size=None, window='boxcar', window_args=()):
    """
    Return the moving mean at each ``a`` element across a width ``window``.

    Similar to ``bottleneck.move_std`` but continues to the end elements and
    allows a choice of window function.
    For one call, performance is similar with and without ``bottleneck``
    installed. For many calls, performance increases by ~3x for a generic
    window, and ~50x for ``window='boxcar'`` with ``bottleneck`` installed.

    Parameters
    ----------
    a : array_like
        Input data to generate moving windows from.
    size : int
        Size of window in number of elements of ``a``.
    window : str, optional
        Window function to use, see ``scipy.signal`` for available functions,
        (default = ``'boxcar'``).
    window_args : tuple, optional
        Any arguments required for the window function e.g. sigma for
        ``window='gaussian'``, (default = ``()``).

    Returns
    -------
    numpy.ndarray
        Array of size ``a.size`` containing the moving stddev of ``a``.
    """
    if size is None:
        size = len(a) // 4

    #if window == 'boxcar':  # A faster method but only for boxcar windows
    #    try:
    #        import bottleneck as bn
    #        # Hopefully this gets the ends correct
    #        np.set_printoptions(threshold=np.nan)
    #        a_extended = _extend_array(a, size)
    #        moving = bn.move_nanstd(a_extended, size, axis=-1)
    #        return moving[np.logical_not(np.isnan(moving))]
    #    except ImportError:
    #        pass

    # Otherwise have to use general window function code
    return np.sqrt(moving_mean(a * a, size, window, window_args) -
                   moving_mean(a, size, window, window_args) ** 2 )

