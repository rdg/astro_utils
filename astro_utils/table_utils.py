"""
This module contains useful functions that deal with reading and saving
tables, and converting between formats of position etc
"""
from __future__ import division, print_function
from math import floor
import os
import re
import sys

import astropy.io.fits as fits
import astropy.io.ascii as ascii
from astropy.table import Table, Column
import numpy as np


def convert_table(input_file, output_file, **kwargs):
    """
    Convert the input file to the output file, format changes allowed

    Parameters
    ----------
    input_file : string
        The path of the table file to convert
    output_file : string
        The path of the table file to convert to
    order_list : list; optional
        Names of columns desired (default=all columns)
    sort_list : list; optional
        Names of columns to sort by, priority decreasing (default=\ ``None``)
    coordinate_format : string; optional
        ``decimal`` or ``sexagesimal``, the desired format (default=input
        format)
    decimal_places : int; optional
        Number of d.p. to return for both RA and Dec string values
        (default=\ ``5``)
    decimal_places_ra : int; optional
        Number of d.p. to return for RA string values
        (default=\ ``decimal_places``)
    decimal_places_dec : int; optiona
        Number of d.p. to return for Dec string values
        (default=\ ``decimal_places``)
    """
    order_list = kwargs.pop('order_list', None)
    sort_list = kwargs.pop('sort_list', None)
    coordinate_format = kwargs.pop('coordinate_format', None)
    decimal_places = kwargs.pop('decimal_places', 5)
    decimal_places_ra = kwargs.pop('decimal_places_ra', None)
    decimal_places_dec = kwargs.pop('decimal_places_dec', None)
    if decimal_places_ra == None:  # don't know why this doesn't work above
        decimal_places_ra = decimal_places
    if decimal_places_dec == None:
        decimal_places_dec = decimal_places

    if kwargs != {}:
        print("Unknown arguments to convert_table:", kwargs)

    # Import table
    table = to_table(file_path=input_file)
    # Sort rows
    if sort_list is not None:
        table.sort(keys=sort_list)
    # Order columns
    table = order_table(table, order_list=order_list)
    # Save table with some coordinate format
    out_file = save_to_file(obj=table, file_name=output_file,
                            save_function=save_table,
                            coordinate_type=None,
                            coordinate_format=coordinate_format,
                            decimal_places=decimal_places,
                            decimal_places_ra=decimal_places_ra,
                            decimal_places_dec=decimal_places_dec)
    return out_file


def _coordinate_to_component_list(value, coordinate_type):
    """ Separate a coordinate string into a list of components

    Parameters
    ----------
    pos : string
        The RA or Dec as a position string (if sexagesimal, requires format
        HHhMMmSSs or [+]DD:MM:SS etc)
    coord : string
        ``ra`` or ``dec``, the type of coordinate

    Returns
    -------
    list of ints,floats
        The components of the position, the length of the list determines the
        format and type of coordinate
        1. RA decimal
        2. Dec decimal
        3. RA sexagesimal
        4. Dec sexagesimal
    """
    components = []
    sign = None
    try:
        if coordinate_type == 'dec':
            sign = '+'
            try:
                if value[0] == '+' or value[0] == '-':
                    sign = value[0]
                    value = value[1:]
            except IndexError:
                if value < 0:
                    sign = '-'
                    value = abs(value)
                raise AttributeError
        if value.find('h') != -1:  # If there is an 'h'
            separator_1 = value.find('h')
            separator_2 = value.find('m')
        elif value.find('d') != -1:
            separator_1 = value.find('d')
            separator_2 = value.find('m')
        elif value.find(':') != -1:
            separator_1 = value.find(':')
            separator_2 = (separator_1 + 1 +
                           value[value.find(':') + 1:].find(':'))
        else:
            raise AttributeError
        components.append(int(value[0: separator_1]))
        components.append(int(value[separator_1 + 1: separator_2]))
        if value.find('s') == -1:
            components.append(float(value[separator_2 + 1:]))
        else:
            components.append(float(value[separator_2 + 1:-1]))
    # Currently in decimal format (could be a float or string)
    except AttributeError:
        components.append(float(value))
    if sign != None:
        components.insert(0, sign)
    return components


def format_coordinate(value, coordinate_type, **kwargs):
    """
    Format a single coordinate into decimal or sexagesimal

    Parameters
    ----------
    value : string, float
        The coordinate to format;
    coordinate_type : string
        Which type ``value`` is: ``'ra'`` or ``'dec'``;
    format : string, optional
        The desired format, ``'decimal'`` or ``'sexagesimal'``
        (default=input format);
    decimal_places : int; optional
        Number of d.p. to return for string values (default=``5``);
    to_string : bool; optional
        Return decimal values as strings (``True``) or floats
        (default=``False``).

    Returns
    -------
    string or float
        The correctly formatted coordinate
    """
    format = kwargs.pop('format', None)
    decimal_places = kwargs.pop('decimal_places', 5)
    to_string = kwargs.pop('to_string', False)
    if kwargs != {}:
        print("Unknown arguments in format_coordinate:", kwargs)

    component_list = _coordinate_to_component_list(value, coordinate_type)

    # Keep coordinate format the same if it is not specified
    if format == None:
        if len(component_list) < 3:
            format = 'decimal'
        else:
            format = 'sexagesimal'

    if format == 'decimal':
        pos = _format_decimal_coordinate(component_list, coordinate_type,
                                         decimal_places=decimal_places,
                                         to_string=to_string             )

    else:
        pos = _format_sexagesimal_coordinate(component_list, coordinate_type,
                                             decimal_places=decimal_places)

    return pos


def format_coordinates(values, **kwargs):
    """
    Format a container of coordinates into decimal or sexagesimal

    Parameters
    ----------
    values : collection, astropy.table.Table or astropy.table.Column
        Possesses an internal container of coordinates
    coordinate_type : string; optional
        ``ra`` or ``dec``, the coordinate involved (default=\ ``None``)
        ``None`` to be used for an astropy.table.Table
    coordinate_format : string; optional
        ``decimal`` or ``sexagesimal``, the desired format
        (default=input format)
    decimal_places : int; optional
        The number of d.p. to output for both RA and Dec string values
        (defaut=\ ``5``)
    decimal_places_ra : int; optional
        Number of d.p. to return for RA string values
        (default=\ ``decimal_places``)
    decimal_places_dec : int; optional
        Number of d.p. to return for Dec string values
        (default=\ ``decimal_places``)
    to_string : bool; optional
        Return decimal values as strings (\ ``True``) or floats
        (default=\ ``False``)

    Returns
    -------
    Input type
        All coordinates in correct format
    """
    coordinate_type = kwargs.pop('coordinate_type', None)
    coordinate_format = kwargs.pop('coordinate_format', None)
    decimal_places = kwargs.pop('decimal_places', 5)
    decimal_places_ra = kwargs.pop('decimal_places_ra', None)
    decimal_places_dec = kwargs.pop('decimal_places_dec', None)
    if decimal_places_ra == None:  # don't know why this doesn't work above
        decimal_places_ra = decimal_places
    if decimal_places_dec == None:
        decimal_places_dec = decimal_places
    to_string = kwargs.pop('to_string', False)
    #if kwargs != {}:
    #    print("Unknown arguments in format_coordinate:", kwargs)
    if isinstance(values, Table):
        formatted_values = Table(values)
        new_ra_col = None
        new_dec_col = None
        for column in values.colnames:
            if column.lower() == 'ra':
                new_ra_col = format_coordinates(values[column],
                                        coordinate_type='ra',
                                        coordinate_format=coordinate_format,
                                        decimal_places=decimal_places_ra,
                                        to_string=to_string)
            elif column.lower() == 'dec':
                new_dec_col = format_coordinates(values[column],
                                        coordinate_type='dec',
                                        coordinate_format=coordinate_format,
                                        decimal_places=decimal_places_dec,
                                        to_string=to_string)
        # replace ra and dec columns with new columns
        try:
            formatted_values.add_columns([new_ra_col, new_dec_col],
                                [values.colnames.index(new_ra_col.name[:-4]),
                                 values.colnames.index(new_dec_col.name[:-4])])
            formatted_values.remove_columns([new_ra_col.name[:-4],
                                             new_dec_col.name[:-4]])
            formatted_values.rename_column(new_ra_col.name, 'RA')
            formatted_values.rename_column(new_dec_col.name, 'Dec')
        except AttributeError:
            pass
    else:  # Collection or astropy.table.Column
        formatted_values = []
        for index in range(len(values)):
            formatted_values.append(format_coordinate(values[index],
                                        coordinate_type,
                                        coordinate_format=coordinate_format,
                                        decimal_places=decimal_places,
                                        to_string=to_string))
        if type(values) == np.ndarray:
            formatted_values = np.array(formatted_values)
        elif type(values) == Column:
            try:
                if (formatted_values[0][0] == '+' or
                        formatted_values[0][0] == '-'):
                    units = 'dms'
                else:
                    units = 'hms'
            except TypeError:
                units = 'degrees'
            formatted_values = Column(values.name + '_new',
                                      formatted_values, unit=units)
    return formatted_values


def _format_decimal_coordinate(component_list, coordinate_type, **kwargs):
    """
    Format a single coordinate into a decimal format

    Parameters
    ----------
    component_list : list
        A list of the components of the coordinate, such as produced by
        ``_coordinate_to_component_list``
    coordinate_type : string
        ``ra`` or ``dec``
    decimal_places : int; optional
        The number of d.p. to output for string values (default=\ ``5``)
    to_string : bool; optional
        Return decimal values as strings (\ ``True``) or floats (\ ``False``)

    Returns
    -------
    string or float
        The correctly formatted coordinate
    """
    decimal_places = kwargs.pop('decimal_places', 5)
    to_string = kwargs.pop('to_string', False)
    list_len = len(component_list)
    if kwargs != {}:
        print("Unknown arguments in _format_decimal_coordinate:", kwargs)

    # Determine components of string
    if list_len == 1:  # ra from decimal
        contents = component_list[0]
    elif list_len == 2:  # dec from decimal
        sign = component_list[0]
        contents = component_list[1]
    elif list_len == 3:  # ra from sexagesimal
        contents = (3600 * component_list[0] + 60 * component_list[1]
                    + component_list[2]) / 240.0
    elif list_len == 4:  # dec from sexagesimal
        sign = component_list[0]
        contents = component_list[1] + \
                   (60 * component_list[2] + component_list[3]) / 3600.0
    # String formatting
    if coordinate_type == 'ra':
        front_length = decimal_places + 4
        pos = "{0:0{1}.0{2}f}".format(contents, front_length,
                                           decimal_places)
    else:
        front_length = decimal_places + 3
        pos = "{0}{1:0{2}.0{3}f}".format(sign, contents, front_length,
                                           decimal_places)
    if to_string == False:
        pos = float(pos)
    return pos


def _format_sexagesimal_coordinate(component_list, coordinate_type, **kwargs):
    """
    Format a single coordinate into a sexagesimal format

    Required arguments:
    component_list: list, A list of the components of the coordinate, such as
                    produced by _coordinate_to_component_list
    coordinate_type: string, 'ra' or 'dec'

    Optional arguments:
    decimal_places: int, Default = 5
        The number of d.p. to output for string values
    to_string: bool, Default = False
        return decimal values as strings (True) or floats (False)

    Returns
    string, the correctly formatted coordinate
    """
    decimal_places = kwargs.pop('decimal_places', 5)
    list_len = len(component_list)
    if kwargs != {}:
        print("Unknown arguments in _format_sexagesimal_coordinate:", kwargs)

    if list_len == 1:  # ra from decimal
        contents_0 = int(floor(component_list[0] / 15))
        contents_1 = int(floor(4 * component_list[0] - 60 * contents_0))
        contents_2 = 240 * component_list[0] - 3600 * contents_0 - \
                     60 * contents_1
    elif list_len == 2:  # dec from decimal
        sign = component_list[0]
        contents_0 = int(floor(component_list[1]))
        contents_1 = int(floor(60 * (component_list[1] - contents_0)))
        contents_2 = 3600 * (component_list[1] - contents_0) - \
                     60 * contents_1
    elif list_len == 3:  # ra from sexagesimal
        contents_0 = component_list[0]
        contents_1 = component_list[1]
        contents_2 = component_list[2]
    elif list_len == 4:  # dec from sexagesimal
        sign = component_list[0]
        contents_0 = component_list[1]
        contents_1 = component_list[2]
        contents_2 = component_list[3]
    if coordinate_type == 'ra':
        front_length = decimal_places + 3
        pos = "{0:02d}:{1:02d}:{2:0{3}.0{4}f}".format(contents_0,
                   contents_1, contents_2, front_length, decimal_places)
    else:
        front_length = decimal_places + 3
        pos = "{0}{1:02d}:{2:02d}:{3:0{4}.0{5}f}".format(sign, contents_0,
                  contents_1, contents_2, front_length, decimal_places)
    return pos


def _lst_to_table(file_path):
    """
    Convert a ``.lst`` file to an ``astropy.table.Table``

    Arguments:
    file_path: string, The path to the file to input

    Returns:
    astropy.table.Table, The resulting table
    """
    # Should compile these regexs
    ra_re = re.compile(r'([012]?[0-9][ :h]+[0-9]{1,2}[ :m]+[0-9]{1,2}\.?'
                        r'[0-9]*[s]?(?=[, \n]))|'
                        r'([0-9]{1,3}\.?[0-9]*(?=[, \n]))')
    dec_re = re.compile(r'([+-]?[0-9]{1,2}[ :d]+[0-9]{1,2}[ :m]+[0-9]{1,2}\.?'
                        r'[0-9]*[s]?(?=[, \n]))|'
                        r'([+-]?[0-9]{1,2}\.?[0-9]*(?=[, \n]))')
    key_re = re.compile(r'[a-zA-Z0-9]*(?=:)')

    file_in = open(file_path, 'r').readlines()

    targets = {'Name': [], 'RA': [], 'Dec': [], 'Notes': []}
    target_number = -1
    line_index = 0
    while  line_index < len(file_in):
        try:
            coordinates = ['', '']
            coordinates[0] = ra_re.search(file_in[line_index]).group()
            coordinates[1] = dec_re.search(re.sub(coordinates[0], '',
                                               file_in[line_index])).group()
            target_number += 1
            targets['RA'].append(coordinates[0])
            targets['Dec'].append(coordinates[1])
        except AttributeError:
            line_index += 1
            continue

        block_start = line_index
        while (file_in[block_start] != '\n' and block_start > -1):
            block_start -= 1
        block_start += 1
        block_end = line_index
        try:
            while (file_in[block_end] != '\n' and
                    block_end < len(file_in)):
                block_end += 1
        except IndexError:
            block_end = len(file_in)

        line_index = block_end + 1

        # Now extract target details from the block
        # Assume target name is the first line of the block
        targets['Name'].append(file_in[block_start][:-1])
        targets['Notes'].append('')
        for block_line in file_in[block_start + 1:block_end]:
            # If line contains coordinates then skip
            if block_line.find(targets['RA'][target_number]) != -1:
                continue
            try:
                key = key_re.search(''.join(block_line.split())).group()
                value = ''.join(block_line.split())[(len(key) + 1):]
                try:
                    targets[key].append(value)
                except KeyError:
                    targets[key] = [''] * target_number + [value]
            except AttributeError:
                targets['Notes'][target_number] += block_line[:-1]
        # Now appended all relevant data to targets, need to append '' entries
        for key in targets:
            if len(targets[key]) != (target_number + 1):
                targets[key].append('')
    #table = Table()
    output_order = (['Name', 'RA', 'Dec'] + [''] * (len(targets) - 4)
                                                                   + ['Notes'])
    output_order[3:-1] = sorted([key for key in targets if key not in
                                ['Name', 'RA', 'Dec', 'Notes']], key=str.lower)
    return Table([Column(key, targets[key]) for key in output_order])


def next_file_name(file_name):
    """
    Find the next available file name, suffixed with ``_n`` where ``n`` is a
    number

    Parameters
    ----------
    file_name : string
        The original file name

    Returns
    -------
    string
        The next available name
    """
    counter = 1
    next_name = file_name
    while os.path.isfile(next_name):
        root, ext = os.path.splitext(file_name)
        root += ('_' + str(counter))
        next_name = ''.join((root, ext))
        counter += 1
    return next_name


def order_table(table, order_list=None):
    """
    Return a new table from columns specified in ``order_list``

    Parameters
    ----------
    table : astropy.table.Table
        The table to take columns from
    order_list : list of strings; optional
        The columns (in order) desired in the returned table
        (default=all columns)

    Returns
    -------
    astropy.table.Table
        A new table created from the specified columns
    """
    if order_list == None:
        return Table(table)
    ordered_table = Table()
    if (len(order_list) < len(table[0]) and
            set(order_list).issubset(set(table.colnames))):
        for column in order_list:
            ordered_table.add_column(table.columns[column])
    else:
        print("Order is not valid")
        ordered_table = table
    return ordered_table


def save_table(table, file_name, **kwargs):  # ===============
    """ Save the table to an output file

    Parameters
    ----------
    table : astropy.table.Table
        The table to be saved
    file_name : string
        The path of the file to save ``table`` to
    overwrite: bool; optional
        Overwrite an existing file (default=\ ``False``)
    coordinate_type : string; optional
        ``ra`` or ``dec``, the coordinate involved (default=\ ``None``).
        ``None`` to be used for an astropy.table.Table
    coordinate_format : string
        ``decimal`` or ``sexagesimal``, the desired coordinate format
        (default=input format)
    decimal_places : int; optional
        Number of d.p. for both RA and Dec string values in the saved table
        (default=\ ``5``)
    decimal_places_ra : int; optional
        Number of d.p. for RA string values in the saved table
        (default=decimal_places)
    decimal_places_dec : int; optional
        Number of d.p. for Dec string values in the saved table
        (default=decimal_places)

    Returns
    -------
    string
        The path of the file to save to
    """
    formatted_table = format_coordinates(table, to_string=True, **kwargs)
    if os.path.isfile(file_name) == True:
    #    if overwrite == False:
    #        raise IOError
        os.remove(file_name)
    if file_name[-4:] == '.cat':
        file_out = open(file_name, 'w')
        formatted_table = _to_cat_string_list(formatted_table)
        for row in formatted_table:
            file_out.write(row + "\n")
    elif file_name[-4:] == '.lst':
        file_out = open(file_name, 'w')
        for row in formatted_table:
            file_out.write(row['Name'] + "\n")
            file_out.write(row['RA'] + " " + row['Dec'] + "\n")
            for column_index in range(len(row)):
                if formatted_table.colnames[column_index] \
                        not in ('Name', 'RA', 'Dec'):
                    if row[column_index] != '':
                        file_out.write(formatted_table.colnames[column_index]
                                       + ": " + str(row[column_index]) + "\n")
            file_out.write("\n")
    else:  # ascii table - doesn't support fits yet ==========================
        ascii.write(formatted_table, file_name)
    return file_name


def save_to_file(obj, file_name, save_function, **kwargs):
    """
    Save the object to a file using an input functions

    Parameters
    ----------
    obj
        The object to save
    file_name : string
        The output file to be saved to
    save_function : function
        The function that can save ``obj`` to a file
    overwrite : bool; optional
        ``True``: overwrite existing file with no confirmation, ``False``: ask
        for confirmation if ``file_name`` exists (default=\ ``False``)
    Any required by save_function

    Returns
    -------
    Return of save_function
    """
    overwrite = kwargs.pop('overwrite', None)
    if overwrite is True:
        pass
    elif overwrite is False:
        file_name = next_file_name(file_name)
    else:
        if os.path.isfile(file_name):
            while True:
                if sys.version_info[0] > 2:
                    overwrite_input = input("File exists: overwrite? [y/n]  ")
                else:
                    overwrite_input = raw_input("File exists: overwrite? "
                                                "[y/n]  ")
                if overwrite_input.lower() in ('y', 'ye', 'yes'):
                    break
                elif overwrite_input.lower() in ('n', 'no'):
                    file_name = next_file_name(file_name)
                    break
                else:
                    print("Please type [y/n]")
    saved_name = save_function(obj, file_name, **kwargs)
    print("Saved to", saved_name)
    return saved_name


def _to_cat_string_list(table):
    """
    Convert an ``astropy.table.Table`` into a list of strings (one for each
    row)

    Parameters
    ----------
    table : astropy.table.Table
        The table to be converted

    Returns
    -------
    list of strings
        The string representation of each row of the table
    """
    # Find maximum column widths so that they can be nicely aligned
    max_lengths = [max([len(table.colnames[column_index])] +
                       [len(str(table[row_index][column_index]).strip())
                        for row_index in range(len(table))])
                   for column_index in range(len(table[0]))]
    linestrings = [''] * (len(table) + 1)
    # Fill in column names
    for column in range(len(table[0])):
        linestrings[0] += str(table.colnames[column])
        # Leave a 2 space gap after each column
        linestrings[0] += (' ' * (max_lengths[column] -
                           len(str(table.colnames[column])) + 2))
    # Fill in column contents
    for row in range(len(table)):
        for column in range(len(table[0])):
            if str(table[row][column]) == '':
                linestrings[row + 1] += '-'
                linestrings[row + 1] += ' ' * (max_lengths[column] + 1)
            else:
                linestrings[row + 1] += re.sub(' ', '_',
                                               str(table[row][column]).strip())
                linestrings[row + 1] += (' ' * (max_lengths[column] -
                                    len(str(table[row][column]).strip()) + 2))
    return linestrings


def to_table(file_path):
    """
    Convert an input file into an ``astropy.table.Table``

    Parameters
    ----------
    file_path : string
        The path to the file to input

    Returns
    -------
    astropy.table.Table
        The resulting table
    """
    try:
        if file_path[-4:] == '.fits':
            table = Table(fits.getdata(file_path, 1))
        else:
            try:
                table = Table(ascii.read(file_path))
            except ascii.InconsistentTableError:
                table = _lst_to_table(file_path)
    except IOError:
        print("Input file is not valid")
        sys.exit(1)

    # Tidy up table by removing '-'s
    for row in range(len(table)):
        for column in range(len(table[0])):
            if table[row][column] == '-':
                table[row][column] = ''

    return table
