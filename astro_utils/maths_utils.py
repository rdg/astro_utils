"""
This module contains useful mathematical functions such as to calculate the
error on a scalar returned by a multi-variable function.
"""
import collections
import copy

import astropy.constants.si as con
from astropy.nddata import NDData
import astropy.units as u
import numpy as np

from astro_utils import moving


def area_gaussian(amplitude, sigma):
    return np.sqrt(2 * np.pi) * amplitude * sigma


def area_sinc(amplitude, sigma):
    return np.pi * amplitude * sigma


def bootstrap(a, n, func=None, samples=1000):
    """ """
    bootstrapped_samples = np.random.choice(a.compressed(),
                                            (samples, n)   )
    if func:
        return func(bootstrapped_samples, axis=1)
    return bootstrapped_samples


def covariance(function, optimal_params, cov_params, args=None, kwargs=None):
    """
    The covariace of a container returned by ``function(*optimal_parameters)``.

    Parameters
    ----------
    function : function
        The function accepting `optimal_parameters`, must return a scalar.
    optimal_params : array_like
        The set of input parameters passed to `function` that the error is
        wished to be found for.
    cov_params : array_like
        The covariance between each value in `optimal_parameters`, must be an
        array with dimensions n * n, where n is
        ``len(list(flatten(optimal_params)))``.
        This can be created from a 1D array of errors by
        ``np.diag(errors ** 2)
    args : iterator, optional
        Any other parameters to pass to `function`, that will not be varied,
        such it is called as ``function(*optimal_parameters, *args)``,
        (default=``None``).
    kwargs : dictionary, optional
        Any other parameters to pass to `function`, that will not be varied,
        such it is called as ``function(*optimal_parameters, **kwargs)``,
        (default=``None``).

    Returns
    -------
    numpy.ndarray :
        The covariance of the optimal returns of function.
    """

    # Get f(x, y)
    if args is None:
        if kwargs is None:
            optimal_func_return = function(*optimal_params)
        else:
            optimal_func_return = function(*optimal_params, **kwargs)
    else:
        if kwargs is None:
            optimal_func_return = function(*(list(optimal_params) + list(args))
                                           )
        else:
            optimal_func_return = function(*(list(optimal_params)
                                             + list(args)        ),
                                           **kwargs                )

    # Vary each parameter with a small dx, even those within nested iterables
    cov_params = np.array(cov_params)
    variations = copy.deepcopy(optimal_params)
    for index, iterable in nested_access(variations):
        iterable[index] = np.sqrt(cov_params[index, index]) * 0.01 #1e-3

    # Vary each parameter and get f(x + dx, y), f(x, y + dy) etc
    vary = vary_parameters(optimal_params, variations)

    varied_func_return = []
    for varied_parameters in vary:  # vary needs to be 3d if an array is passed

        if args is None:
            if kwargs is None:
                result = function(*varied_parameters)
            else:
                result = function(*varied_parameters, **kwargs)
        else:
            if kwargs is None:
                result = function(*(list(varied_parameters) + list(args)))
            else:
                #result = function(varied_parameters, *args, **kwargs)
                result = function(*(varied_parameters + args), **kwargs)
        try:
            result = [float(i) for i in result]
        except TypeError:  # single returned result
            result = float(result)
        varied_func_return.append(result)

    # Change varied_func_return and optimal_func_return to 2D arrays,
    # containing one row for each returned value
    varied_func_return = np.transpose(varied_func_return)
    optimal_func_return = np.array(optimal_func_return)
    try:
        optimal_func_return = \
            optimal_func_return.reshape((len(optimal_func_return), 1))
    except TypeError:  # function only returns 1 value
        pass

    # Flatten variations and error params, to a 1D array with one element per
    # variable parameter
    variations = np.ravel(variations)

    # Construct the jacobian, given that df/dx = (f(x + dx, y) - f(x, y)) / dx
    jacobian = (varied_func_return - optimal_func_return) / variations
    cov_params = np.array(cov_params)
    return jacobian.dot(np.array(cov_params).dot(np.transpose(jacobian)))


def find_peaks(a, significance=3, size=None, window='boxcar', window_args=()):
    """
    Return the indices of ``a`` at which ``a > significance * moving_std(a)``.

    Parameters
    ----------
    a : array_like
        The array to find peaks within.
    significance : float, optional
        The significance (in terms of ``moving.moving_std(a)``) above which
        values count as peaks.
    size : int, optional
        Size of window in number of elements of ``a`` (default=``len(a)//4``).
    window : str, optional
        Window function to use, see ``scipy.signal`` for available functions,
        (default = ``'boxcar'``).
    window_args : tuple, optional
        Any arguments required for the window function e.g. sigma for
        ``window='gaussian'``, (default = ``()``).

    Returns
    -------
    ndarray
        The indices of ``a`` determined as significant peaks
    """
    a_std = moving.moving_std(a, size=size,
                              window=window, window_args=window_args)
    return np.where(a > (significance * a_std))


def fit_gaussian(x_data, y_data, p0=None, **kwargs):
    """
    No support for units yet - x_data, y_data, p0 must be scalars
    p0 can also be a list of lists of paramaters for multi-component gaussians
    """
    restrictions = kwargs.pop('restrictions', None)

    if p0 is None:
        x_len = len(x_data)
        mean = x_data[x_len / 2]
        sigma = x_data[x_len / 2 + 5] - x_data[x_len / 2]
        amplitude = y_data[x_len / 2]
        base = 0
        p0 = [[mean, sigma, amplitude], base]

    # curve_fit only operates on 1D p0 iterables
    flatp0 = list(flatten(p0))

    def restricted_gaussian(disp, *args):
        return gaussian(disp, *args, restrictions=restrictions, p0=flatp0)

    from scipy.optimize import curve_fit
    popt, pcov = curve_fit(restricted_gaussian,
                           x_data, y_data, p0=flatp0, **kwargs)

    # Return the optimal values of p0 in the same shape it was input
    import copy
    optimal = copy.deepcopy(list(p0))
    popt_index = 0
    for optimal_index, iterable in nested_access(optimal):
        iterable[optimal_index] = popt[popt_index]
        popt_index += 1
    return optimal


def fit_sinc(x_data, y_data, p0=None, **kwargs):
    """
    """
    restrictions = kwargs.pop('restrictions', [1000, 1000, 1000, 1000])
    if p0 is None:
        mid_index = int(len(x_data) / 2)
        mean = x_data[mid_index]
        sigma = x_data[mid_index + 5] - x_data[mid_index]
        amplitude = y_data[mid_index]
        base = 0
        p0 = [[mean, sigma, amplitude], base]

    # curve_fit only operates on 1D p0 iterables
    flatp0 = np.array(list(flatten(p0)))
    restrictions = np.array(restrictions)
    restricted_flatp0 = flatp0[restrictions != 0]
    restricted_restrictions = restrictions[restrictions != 0]

    def restricted_sinc(disp, *restricted_params):
        # Replace the restricted flatp0 values with curve_fits guesses
        if (np.abs(restricted_params - restricted_flatp0)
                                            > restricted_restrictions).any():
            return 10000 * np.ones(disp.shape)
        params = flatp0.copy()
        params[restrictions != 0] = np.array(restricted_params)
        return sinc(disp, params)

    #def restricted_sinc(disp, *args):
    #    return sinc(disp, *args, restrictions=restrictions, p0=flatp0)

    from scipy.optimize import curve_fit
    popt, pcov = curve_fit(restricted_sinc,
                           x_data, y_data, p0=restricted_flatp0, **kwargs)

    #from scipy.optimize import minimize
    #def chi_squared(*sinc_params):
    #    return np.sum((y_data - sinc(x_data,
    #                                 *sinc_params,
    #                                 restrictions=restrictions,
    #                                 p0=flatp0
    #                                 )) ** 2,
    #                   #axis=0,
    #                   )

    #result = minimize(chi_squared,
    #                  x0=flatp0,
    #                  **kwargs
    #                  )
    #popt = result.x
    #chi_square = result.fun

    #popt, pcov = curve_fit(restricted_sinc,
    #                       x_data, y_data, p0=flatp0, **kwargs)

    # Return the optimal values of p0 in the same shape it was input

    import copy
    optimal = copy.deepcopy(list(p0))
    flat_optimal = flatp0.copy()
    flat_optimal[restrictions != 0] = popt
    for popt_index, (optimal_index, iterable) in enumerate(
                                                    nested_access(optimal)):
        iterable[optimal_index] = flat_optimal[popt_index]

    uncertainties = copy.deepcopy(list(p0))
    flat_uncertainties = np.zeros(len(flatp0))
    flat_uncertainties[restrictions != 0] = np.sqrt(np.diag(pcov))
    for std_index, (uncertainty_index, iterable) in enumerate(
                                                nested_access(uncertainties)):
        iterable[uncertainty_index] = flat_uncertainties[std_index]

    return optimal, uncertainties


def flatten(iterable):
    """
    Generator, the elements of a nested container flattened to depth 1.

    E.g. given [[1, 2], 3, [4, [5, 6], 7]], returns the elements
    [1, 2, 3, 4, 5, 6, 7].

    Parameters
    ----------
    iterable : a container
        The (possibly nested) container to flatten.

    Returns
    -------
    elements of `iterable` :
        The next element of the flattened container.
    """
    for element in iterable:
        if (isinstance(element, collections.Iterable)
                and not isinstance(element, str)     ):
            # Recursively flatten `element`
            for next_level_element in flatten(element):
                yield next_level_element
        else:  # element is not an iterable
            yield element


def gaussian(disp, *args, **kwargs):
    """
    """
    # *args, p0 can be a list of many mean, sigma, amplitude, groups,
    # followed by base
    # Need to convert to list such that args has a size of at least 1
    args = np.array(list(flatten(args)))

    # When fitting, can restrict e.g. mean to within restrictions[0] of p0[0]
    # Doesn't work well for small values of restriction
    restrictions = kwargs.pop('restrictions', None)

    if restrictions is not None:
        p0 = np.array(kwargs.pop('p0', []))
        restrictions = np.array(restrictions)
        # If any values of restrictions are 0
        # Change args to p0 for that value - such that changes have no effect
        args[restrictions == 0] = p0[restrictions == 0]
        # Change restrictions for those values to 1, such that below works
        restrictions[restrictions == 0] = 1

        # If args is outside restricted range, force fitter to ignore this
        #print("p0 =", p0)
        #print("restrictions =", restrictions)
        #print("p0 - restrictions =", p0 - restrictions)
        #print("< args =", (p0 - restrictions) < args)
        #print("all =", np.all((p0 - restrictions) < args))
        if not (np.all((p0 - restrictions) < args) and
                np.all(args < (p0 + restrictions))    ):
            return args[2] * 1e10

    # Total is the sum of each gaussian at that disp
    total = 0
    guess = (args[start: start + 3] for start in range(0, len(args) - 1, 3))
    for group in guess:
        total += (group[2] * np.exp(-((disp - group[0]) ** 2)
                                    / (2 * group[1] ** 2)    ))

    # Add base and return
    return total + args[-1]


def ghz_to_km_per_s(ghz_value, velocity_zero_point):
    """
    Convert from gigahertz to km/s given the frequency at 0 km/s.

    Can be used as a transformation function for
    ``astro_utils.plot_utils.add_upper_axis()``.

    Parameters
    ----------
    ghz_value : array_like
        The value(s) to convert.
    ghz_vel_zero_point : float
        The frequency (in GHz) at 0 km/s

    Returns
    -------
    ndarray :
        The corresponding value(s) in km/s
    """
    try:
        ghz_value = ghz_value.value
    except AttributeError:
        if type(ghz_value) is NDData:
            ghz_value = ghz_value.data
    try:
        ghz_vel_zero_point = to_unit(u.GHz, velocity_zero_point)
        ghz_vel_zero_point = ghz_vel_zero_point.value
    except AttributeError:
        pass
    return (con.c * 0.001 * (ghz_vel_zero_point ** 2 - ghz_value ** 2)
                          / (ghz_vel_zero_point ** 2 + ghz_value ** 2)).value


def ghz_to_um(ghz_value):
    """
    Convert from gigahertz to micrometres

    Can be used as a transformation function for
    ``astro_utils.plot_utils.add_upper_axis()``.

    Parameters
    ----------
    ghz_value : array_like
        The value(s) to convert.

    Returns
    -------
    ndarray
        The corresponding value(s) in micrometres.
    """
    try:
        return ghz_value.convert_unit_to(u.um, equivalencies=u.spectral())
    except AttributeError:
        try:
            return ghz_value.to(u.um, equivalencies=u.spectral())
        except AttributeError:  # float, ndarray or similar
            return (con.c * 1e-3 / np.array(ghz_value, dtype=np.float64)).value


def incomplete_riemann_zeta(freq, gamma, minimum_temperature):
    """
    Scipy doesn't provide an incomplete Riemann zeta function,
    so use integration instead to do it manually
    scipy.integrate.quad can't take an array of upper limits, so have
    to create a function and use a (slow) loop.
    """
    from scipy.integrate import quad
    incomplete_riemann_zeta_integrand = \
        lambda x, gamma: x ** (gamma - 2) / (np.exp(x) - 1)
    all_integrals = []
    for frequency in freq:
        integral, uncertainty = quad(incomplete_riemann_zeta_integrand,
                               0,  # Lower integration limit
                               con.h.value * frequency /  # Upper
                                   (con.k_B.value * minimum_temperature),
                               args=(gamma))
        all_integrals.append(integral)
    return np.array(all_integrals)


def jackknife(a):
    """
    Return jackknife samples of input array

    e.g. a = [1, 2, 3, 4]
    return [[2, 3, 4], [1, 3, 4], [1, 2, 4], [1, 2, 3]]

    Parameters
    ----------
    a : array_like
        1 or 2d (possibly 3 but haven't tested) dataset to return jackknife
        samples from

    Returns
    -------
    numpy.ma.masked_array :
        Jackknife samples
    """
    jackknife_sets = np.ma.masked_array(
            data=np.tile(a, [len(a)] + [1] * len(a.shape)),
            mask=np.repeat(np.reshape(np.diag(np.ones(len(a))),
                                      [len(a)] * 2 + [1] * (len(a.shape) - 1)),
                           len(np.array(a[0], ndmin=1)),
                           axis=-1
                           )
            )
    return jackknife_sets


def km_per_s_to_ghz(km_per_s_value, velocity_zero_point):
    """
    Convert from km/s to gigahertz given the frequency at 0 km/s.

    Can be used as a transformation function for
    ``astro_utils.plot_utils.add_upper_axis()`` if wrapped, e.g.::

        def km_per_s_to_ghz(km_per_s_value):
            return astro_utils.spec_utils.km_per_s_to_ghz(km_per_s_value, 1)

        fig.add_upper_axis(ghz_to_km_per_s, km_per_s_to_ghz)

    Parameters
    ----------
    km_per_s_value : array_like
        The value(s) to convert.
    velocity_zero_point : float/astropy.quantity.Quantity
        The quantity/frequency(in GHz) at 0 km/s

    Returns
    -------
    ndarray :
        The corresponding value(s) in GHz
    """
    try:
        km_per_s_value = km_per_s_value.value
    except AttributeError:
        pass
    try:
        ghz_vel_zero_point = to_unit(u.GHz, velocity_zero_point)
        ghz_vel_zero_point = ghz_vel_zero_point.value
    except AttributeError:
        pass

    beta = 1000 * np.asarray(km_per_s_value) / con.c.value
    return ghz_vel_zero_point * np.sqrt((1 - beta) / (1 + beta))


def km_per_s_to_um(km_per_s_value, velocity_zero_point):
    """
    Convert from km/s to micrometres given the frequency at 0 km/s.

    Can be used as a transformation function for
    ``astro_utils.plot_utils.add_upper_axis()`` if wrapped, e.g.::

        def km_per_s_to_ghz(km_per_s_value):
            return astro_utils.spec_utils.km_per_s_to_ghz(km_per_s_value, 1)

        fig.add_upper_axis(ghz_to_km_per_s, km_per_s_to_ghz)

    Parameters
    ----------
    km_per_s_value : array_like
        The value(s) to convert.
    velocity_zero_point : float/astropy.quantity.Quantity
        The quantity/frequency(in GHz) at 0 km/s

    Returns
    -------
    ndarray :
        The corresponding value(s) in GHz
    """
    return ghz_to_um(km_per_s_to_ghz(km_per_s_value, velocity_zero_point))


def nested_access(iterable):
    """
    Generator, accessors to the elements of a possibly nested container.

    Returns index, accessor_iterable such that accessor_iterable[index] allows
    access to elements within a nested container

    Parameters
    ----------
    iterable : a container
        The (possibly nested) container to access.

    Returns
    -------
    index, accessor_iterable :
        The accessor to the next element within the container
    """
    for index, element in enumerate(iterable):
        if (isinstance(element, collections.Iterable)
                and not isinstance(element, str)     ):
            # Recursively access `element`  (inner_iterable == element)
            for inner_index, inner_iterable in nested_access(element):
                yield inner_index, inner_iterable
        else:  # `element` is not an iterable
            yield index, iterable


def sinc(disp, *args, **kwargs):
    """
    disp, [[mean, sigma, amplitude], [mean, sigma, amplitude], base]
    """
    # *args, p0 can be a list of many mean, sigma, amplitude, groups,
    # followed by base
    args = np.array(list(flatten(args)))

    # When fitting, can restrict e.g. mean to within restrictions[0] of p0[0]
    # Doesn't work well for small values of restriction
    restrictions = kwargs.pop('restrictions', None)

    if restrictions:
        p0 = np.array(kwargs.pop('p0', []))
        restrictions = np.array(restrictions)
        # If any values of restrictions are 0
        # Change args to p0 for that value - such that changes have no effect
        args[restrictions == 0] = p0[restrictions == 0]
        # Change restrictions for those values to 1, such that below works
        restrictions[restrictions == 0] = 1

        # If args is outside restricted range, force fitter to ignore this
        #print("p0 =", p0)
        #print("restrictions =", restrictions)
        #print("p0 - restrictions =", p0 - restrictions)
        #print("< args =", (p0 - restrictions) < args)
        #print("all =", np.all((p0 - restrictions) < args))
        if not (np.all((p0 - restrictions) < args) and
                np.all(args < (p0 + restrictions))    ):
            return args[2] * 1e10

    # Total is the sum of each sinc at that disp
    total = 0
    guess = np.array([args[start: start + 3]
                      for start in range(0, len(args) - 1, 3)])
    for group in guess:
        total += (group[2] *  np.sinc((disp - group[0]) / (np.pi * group[1])))
    # Add base and return
    return total + args[-1]


def to_unit(unit, quantity, velocity_zero_point=None):
    """
    Convert units of `quantity` to `unit`.

    Parameters
    ----------
    disp : array_like (including astropy.nddata.NDData) or Quantity
        The data to convert (with possible ``units`` attribute).

    Returns
    -------
    input_type :
        `quantity` converted to unit.
    """
    try:  # NDData
        output = quantity.convert_unit_to(unit)
    except u.UnitsError:  # Wavelength
        try:
            output = NDData(quantity.unit.to(unit, quantity.data,
                                             equivalencies=u.spectral()),
                            unit=unit
                            )
        except u.UnitsError:  # Velocity
            if quantity.unit.is_equivalent(u.Hz):  # Frequency
                 output = NDData(ghz_to_km_per_s(quantity.data,
                                                 velocity_zero_point))
            elif quantity.unit.is_equivalent(u.m):  # Wavelength
                 output = NDData(um_to_km_per_s(quantity.data,
                                                velocity_zero_point))
            output.unit = u.km / u.s
    except AttributeError:
        try:  # Quantity
            output = quantity.to(unit)
        except u.UnitsError:  # Wavelength
            try:
                output = quantity.to(unit, equivalencies=u.spectral())
            except u.UnitsError:  # Velocity
                if quantity.unit.is_equivalent(u.Hz):  # Frequency
                    output = ghz_to_km_per_s(quantity.value,
                                             velocity_zero_point)
                elif quantity.unit.is_equivalent(u.m):  # Wavelength
                    output = um_to_km_per_s(quantity.value,
                                            velocity_zero_point)
                elif quantity.unit.is_equivalent(u.m / u.s):  # Velocity
                    if unit.is_equivalent(u.Hz):  # Frequency
                        output = km_per_s_to_ghz(quantity.value,
                                                 velocity_zero_point)
                    elif unit.is_equivalent(u.m):  # Wavelength
                        output = km_per_s_to_um(quantity.value,
                                                velocity_zero_point)
                output *= unit
        except AttributeError:  # no unit information
            output = quantity * unit
    return output


def um_to_ghz(um_value):
    """
    Convert from micrometres to gigahertz.

    Can be used as a transformation function for
    `astro_utils.plot_utils.add_upper_axis()`.

    Parameters
    ----------
    um_value : array_like
        The value(s) to convert.

    Returns
    -------
    ndarray :
        The corresponding value(s) in micrometres.
    """
    try:
        return um_value.convert_unit_to(u.GHz, equivalencies=u.spectral())
    except AttributeError:
        try:
            return um_value.to(u.GHz, equivalencies=u.spectral())
        except AttributeError:  # float, ndarray or similar
            return ghz_to_um(um_value)


def um_to_km_per_s(um_value, velocity_zero_point):
    """
    Convert from micrometres to kilometres per second.

    Can be used as a transformation function for
    `astro_utils.plot_utils.add_upper_axis()`.

    Parameters
    ----------
    um_value : array_like
        The value(s) to convert.

    Returns
    -------
    ndarray :
        The corresponding value(s) in micrometres.
    """
    return ghz_to_km_per_s(um_to_ghz(um_value), velocity_zero_point)


def variance_at_pos(a, size=100, window='boxcar', window_args=()):
    """
    Return the variance at each element along the array

    Parameters
    ----------
    a : array_like (1d)
        The array to find the variance of
    size : int, optional
        Size of window in number of elements of ``a`` (default = 100).
    window : str, optional
        Window function to use, see ``scipy.signal`` for available functions,
        (default = ``'boxcar'``).
    window_args : tuple, optional
        Any arguments required for the window function e.g. sigma for
        ``window='gaussian'``, (default = ``()``).

    Returns
    -------
    numpy.ndarray
        Array of length ``len(a)`` containing the moving variance of ``a``.
    """

    # Baseline subtract (approximately)
    a_bs = a - moving.moving_median(a, size)

    # Pass to variance calculator
    var_array = moving.moving_std(a_bs, size, window=window,
                                  window_args=window_args   )
    var_array *= var_array

    return var_array


def vary_parameters(parameters, variations):
    """
    Generator, `parameters` with one value varied each iteration.

    E.g. given ([1, [2, 3], 4], [0.1, [0.2, 0.3], 0.4]), returns:
    [1.1, [2, 3], 4]  [1, [2.2, 3], 4]  [1, [2, 3.3], 4]  [1, [2, 3], 4.4].

    Parameters
    ----------
    parameters : iterator
        The parameters to vary, values must be of number types.
    variations : iterator
        The values to increase `parameters` by, must have a length of at least
        ``len(parameters)``.

    Returns
    -------
    list :
        'parameters' with one value increased by the corresponding `variation`
        value.
    """
    # Use `flatten` to find the true length of the inputs
    for index, value in enumerate(flatten(parameters)):
        # A copy of parameters will be returned for each variation
        parameters_copy = list(copy.deepcopy(parameters))
        # Get accessors to elements even deep within a nested structure
        parameters_accessors = list(nested_access(parameters_copy))
        variations_accessors = list(nested_access(variations))
        # Add the correct variation to the corresponding parameter
        parameters_accessors[index][1][parameters_accessors[index][0]] += \
            variations_accessors[index][1][variations_accessors[index][0]]
        yield parameters_copy


def vary_parameters_1d(parameters, variations):
    """
    Numpy version of vary_parameters - for 1d collections of parameters

    Returns an array where each row is parameters varied by one of variation.
    E.g. given ([1, 2, 3], [0.1, 0.2, 0.3]) returns:
    array([[1.1, 2,   3  ],
           [1,   2.2, 3  ],
           [1,   2,   3.3]])
    """
    return np.tile(parameters, (len(parameters), 1)) + np.diag(variations)
