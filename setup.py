#!/usr/bin/env python

from distutils.core import setup

setup(
    name='astro_utils',
    version='0.2dev',
    description='Useful package for spectra and table manipulation',
    long_description=open('README.rst').read(),
    license='LICENSE.txt',
    author='Richard George',
    author_email='rdg@roe.ac.uk',
    url='https://bitbucket.org/rdg/astro_utils',
    packages=['astro_utils'],
    scripts=['bin/convert_coordinates','bin/convert_table', 'bin/grey_body'],
    package_data ={'astro_utils': ['spectral_lines.txt',
                                   'pacs_aperture_correction.txt',
                                   'pacs_hpf_correction_red.txt',
                                   'pacs_hpf_correction_green.txt']},
    install_requires=[
        "astropy",
        "lineid_plot >= 0.2",
        "matplotlib >= 1.0",
        "numpy",
        "scipy",
    ],
)

