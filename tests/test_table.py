import os
from random import randint

import numpy as np
import pytest

import astro_utils.table_utils as tabu


class TestNextFileName():
    
    def test_no_file(self):
        """ Test that the desired file name is returned if it does not exist"""
        t_file = './1000.txt'
        while os.path.isfile(t_file):
            t_file = './' + str(randint(1000,9999)) + '.txt'
        assert tabu.next_file_name(t_file) == t_file

    def test_multiple_files(self):
        """ Test that the form root_3.ext is returned for _1 and _2 existing"""
        def files_existent(t_file):
            root, ext = os.path.splitext(t_file)
            if (os.path.isfile(t_file) and os.path.isfile(root + '_1' + ext) and
                    os.path.isfile(root + '_2' + ext)):
                return True
            return False

        t_file = './1000.txt'
        while files_existent(t_file):
            t_file = './' + str(randint(1000, 9999)) + '.txt'
        # t_file, t_file_1, t_file_2 do not exist
        root, ext = os.path.splitext(t_file)
        with open(t_file, 'w'):
            pass
        with open(root + '_1' + ext, 'w'):
            pass
        with open(root + '_2' + ext, 'w'):
            pass
        assert tabu.next_file_name(t_file) == (root + '_3' + ext)
        os.remove(t_file)
        os.remove(root + '_1' + ext)
        os.remove(root + '_2' + ext)

    def test_single_file(self):
        """ Test that the form root_1.ext if returned for root.ext existing"""
        t_file = './1000.txt'
        while os.path.isfile(t_file):
            t_file = './' + str(randint(1000,9999))+ '.txt'
        # t_file does not exist
        root, ext = os.path.splitext(t_file)
        with open(t_file, 'w'):
            pass
        assert tabu.next_file_name(t_file) == root + '_1' + ext
        os.remove(t_file)

